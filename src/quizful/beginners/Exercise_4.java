package quizful.beginners;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 10:20
 */
public class Exercise_4 {

    private double x = 2;

    public static void multX(Exercise_4 a, double n) {
        a.setX(a.getX() * n);
    }

    public double getX() {
        return x;
    }

    public void setX(double xn) {
        x = xn;
    }

    public static void trippleValue(double x) {
        x *= 3;
    }

    public static Exercise_4 resetX(Exercise_4 a) {
        a = new Exercise_4();
        return a;
    }

    public static void main(String[] args) {
        int x = 3;
        trippleValue(x);
        Exercise_4 anA = new Exercise_4();
        multX(anA, x);
        resetX(anA);
        x = 0;
        System.out.print(anA.getX());
    }
}

// Программа выведет на экран "6.0" без кавычек
// При вызове trippleValue(x) значение x не меняется, т.к. это переменная простого типа, которая передается по значению.
// При вызове resetX(anA) объект, на который ссылается anA не меняется, т.к. anA - это ссылка на объект, которая передается по значению.