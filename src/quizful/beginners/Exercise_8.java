package quizful.beginners;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 15:26
 */
public class Exercise_8 {

    String s;

    public static void main(String[] args) {
        //String s;
        //System.out.println(s.toUpperCase());
        //Exercise_8 localVsInstance = new  Exercise_8();
        //System.out.println(localVsInstance.s.toUpperCase());
    }
}

// Для успешной компиляции надо явно инициализировать локальную переменную, null вполне сойдет.
// Переменная объекта s будет неявно инициализирована null'ом. NPE хоть и вылетит, но это уже ошибка времени выполнения.