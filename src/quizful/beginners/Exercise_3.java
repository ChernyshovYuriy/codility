package quizful.beginners;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 10:18
 */
public class Exercise_3 {

    int i = getInt();
    int k = 20;

    public int getInt() {
        return k + 1;
    }

    public static void main(String[] args) {
        Exercise_3 t = new Exercise_3();
        System.out.println(t.i + "  " + t.k);
    }
}

// Будет выведено на печать 1 20
// Инициализация полей при создании объекта осуществляется в порядке их объявления.
// В данном примере первым будет инициализироваться поле i. В это время поле k ещё не инициализировано
// (содержит нулевое значение), и выражение k+1 даст единицу. После этого k получит значение 20.