package quizful.beginners;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 10:10
 */
public class Exercise_2 {

    static final String CLASS_NAME = Exercise_2.class.getSimpleName();

    public static void main(String[] args) {
        MyLink b1 = new MyLink();
        MyLink b2 = b1;
        b2.str = "My String";
        System.out.println(CLASS_NAME + " " + b1.str);

        String a1 = "Test";
        String a2 = a1;
        System.out.println(CLASS_NAME + " " + a2);
        a1 = "Not a Test";
        System.out.println(CLASS_NAME + " " + a2);               //2
    }
}

class MyLink {
    public MyLink() {
        str = "New";
    }
    public String str;
}

// My String
// Test
// Test