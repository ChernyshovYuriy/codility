package quizful.beginners;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 15:30
 */
public class Exercise_10 {

    static {
        MyClass m = new MyClass();                  // 1
        System.out.println(m.toString());           // 2
        System.exit(0);
    }
}

class MyClass {
    public String toString() {
        return "MyClass";
    }
}

// Программа выполнится без ошибок и будет выведено "MyClass"

// Пояснение: Последовательность следующая:
// 1) Происходит загрузка класса Exercise_10;
// 2) Происходит выполнение операторов статического блока класса Exercise_10 (Здесь нет ошибок);
// 3) И после этого JVM могла бы попытаться вызвать метод main(), которого в классе MainClass нет.
// Но в статическом блоке произошло завершение программы вызовом System.exit(0).

// Это поведение полностью соответствует спецификации: "The named method is resolved ...
// On successful resolution of the method, the class that declared the resolved method is initialized (§5.5)
// if that class has not already been initialized." Т.е. инициализация класса (выполнение блока статической
// инициализации и т.п.) осуществляется только в случае успешного резолвинга метода