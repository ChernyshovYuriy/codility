package quizful.beginners;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 15:50
 */
public class Exercise_12 {

    List<Integer> list;

    Exercise_12() {
        list = new ArrayList<Integer>();
        someVoid(list);
    }

    void someVoid(List<Integer> l) {
        l.add(0);
        l = null;
    }

    public static void main(String[] args) {
        Exercise_12 test = new Exercise_12();
        System.out.println("Size is: " + test.list.size());
    }
}

// Код откомпилируется и выведет- "Size is: 1" без кавычек