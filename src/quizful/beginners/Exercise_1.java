package quizful.beginners;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 05.05.14
 * Time: 10:10
 */
public class Exercise_1 {

    static final String CLASS_NAME = Exercise_1.class.getSimpleName();

    public static void main(String[] args) {
        Impl impl = new Impl();
        I_A a;
        a = impl;
        a.out1();
        ((I_B) a).out2();              //2

        class A {

        }
    }
}

interface I_A {
    public void out1();
}

interface I_B {
    public void out2();
}

class Impl implements I_A, I_B {     //1

    public void out2() {
        System.out.print(Exercise_1.CLASS_NAME + " 2");
    }

    public void out1() {
        System.out.print(Exercise_1.CLASS_NAME + " 1");
    }
}

// Напечатает "12" без кавычек
// В Java возможна реализация нескольких интерфейсов. Данное приведение типов возможно, так как класс impl реализует
// оба интерфейса.