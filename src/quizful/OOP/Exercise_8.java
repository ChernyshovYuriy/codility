package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 13:25
 */
public class Exercise_8 {

    public static void main(String... args) {

        A8 b1 = new A8();
        A8 b2 = new B8();
        A8 b3 = new C();

        //B8 b4 = b2;        // 1 Ошибка компиляции
        B8 b5 = (B8) b2;   // 2
        A8 b6 = b3;        // 3
        A8 b7 = (A8) b3;   // 4
        //C b8 = b3;         // 5 Ошибка компиляции
        C b9 = (C) b3;     // 6
        A8 b10 = (B8) b3;  // 7

    }
}

class A8 {

}

class B8 extends A8 {

}

class C extends B8 {

}