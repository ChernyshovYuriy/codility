package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 12:56
 */
public class Exercise_5 implements Inter<String> {     // 1

    public static void main(String[] args) {
        new Exercise_5().fun();
    }

    public String fun() {
        System.out.println("B");
        return null;
    }
}

//class C extends Exercise_5 implements Inter<Boolean> {  // 2
//
//}

interface Inter<T> {
    T fun();
}

// Убрать "implements Inter<Boolean>" из строки //2
// Убрать "extends Exercise_5" из строки //2 и добавить реализацию метода fun в класс C

// Пояснение: Код не компилируется, так как класс не может реализовывать
// два_различных_варианта_параметризованного_интерфейса, в данном случае это Inter<String> и Inter<Boolean>.
// Вариант Убрать "implements Inter<String>" из строки //1 - тоже не подходит, так как в этом случае
// в классе C должен быть метод fun, возвращающий Boolean, но в классе Exercise_5 уже есть метод с такой сигнатурой,
// возвращающий String.