package quizful.OOP;

import java.util.Set;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 12:46
 */
public class Exercise_2 {

    public class Department {
        private int departmentId;
        private Set<Employee> employees;
    }

    public class Employee {
        private int employeeId;
        private String employeeName;
    }
}

// Каким термином лучше всего описать отношение между классами Department и Employee: Агрегация

// Ответ был: композиция, и он неверный
// Пояснение: Композиция описывает сильное отношение между объектами (синоним - "состоит из"): один объект является
// неотъемлемой частью другого. Агрегация же описывает более слабое отношение (синоним - "содержит").