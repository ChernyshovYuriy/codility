package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 12:44
 */
public class Exercise_1 {

    class A {
        public Single method() {
            return new Single();
        }
    }

    class Single extends A {
        //public A method() {
        //    return new Single();
        //}
    }
}

// This code will not compile because public A method() return incorrect object type