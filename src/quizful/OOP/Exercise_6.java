package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 13:00
 */
abstract class Aa {
    int a = 8;

    public Aa() {
        show();
    }

    abstract void show();
}

public class Exercise_6 extends Aa {
    int a = 90;

    void show() {
        System.out.println("" + a);
    }

    public static void main(String args[]) {
        new Exercise_6();
    }
}

// Будет напечатано "0" без кавычек

// Пояснение: При вызове метода show из конструктора базового класса, поле a еще не будет проинициализировано