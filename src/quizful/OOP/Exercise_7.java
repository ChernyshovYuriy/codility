package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 13:22
 */
public class Exercise_7 {

    static class A extends B {

        static Integer q = 2;

        static {
            System.out.print("A");
            A.q = 4;
        }
    }

    static class B {
        static {
            System.out.print("B");
            A.q++;
        }
    }

    public static void main(String[] args) {
        System.out.println(A.q);
    }
}

// Ошибка времени выполнения

// Первым, до инициализации класса "A", происходит инициализация суперкласса "B". У которого в статическом
// инициализаторе вызывается инкремент не инициализированного поля класса "A".
// Что вызывает ExceptionInInitializerError Caused by NullPointerException.