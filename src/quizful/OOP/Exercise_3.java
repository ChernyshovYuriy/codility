package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 12:48
 */
class A {
    String name = "a ";

    String test() {
        return "test A ";
    }
}

class B extends A {
    String name = "b ";

    String test() {
        return "test B ";
    }
}

public class Exercise_3 {
    public static void main(String[] args) {
        new Exercise_3().go();
    }

    void go() {
        A m = new B();
        System.out.println(m.name + m.test());
    }
}

// Какой результат будет получен после компиляции и выполнения данного кода: a test B

// Пояснение: Полиморфизм распространяется только на методы