package quizful.OOP;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 07.05.14
 * Time: 12:52
 */
class Exercise_4 {
    int getX() { return x; }

    int y = getX();
    int x = 3;

    public static void main (String s[]) {
        Exercise_4 p = new Exercise_4();
        System.out.println(p.x + "," + p.y);
    }
}

// 3,0

// При инициализации переменной y был использован результат метода getX(), который вернул значение по умолчанию
// переменной x, то есть 0. Затем переменная x получила значение 3.