/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 11:15 PM
 */
public class MagnitudePole {

    public static int magnitudePole(int[] A) {
        boolean result = true;
        for (int i = 0; i < A.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if ((A[i] - A[j]) < 0) {
                    result = false;
                    break;
                } else {
                    result = true;
                }
            }
            for (int j = i + 1; j < A.length && result; j++) {
                if ((A[j] - A[i]) < 0) {
                    result = false;
                    break;
                }
            }
            if (result) {
                System.out.println("MagnitudePole is " + i);
                return i;
            }
        }
        System.out.println("MagnitudePole is -1");
        return -1;
    }
}