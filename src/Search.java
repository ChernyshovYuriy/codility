import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/13/13
 * Time: 11:36 PM
 */
public class Search {

    public static int binarySearchLongIterable(long[] list, long value) {
        assert list != null : "list can't be null";
        int lowerIndex = 0;
        int upperIndex = list.length - 1;
        while (lowerIndex < upperIndex) {
            int index = lowerIndex + (upperIndex - lowerIndex) / 2;
            if (value == list[index]) {
                return index;
            } else if (value < list[index]) {
                upperIndex = index; //index - 1;
            } else {
                lowerIndex = index + 1;
            }
        }
        return lowerIndex; //-(lowerIndex + 1);
    }

    public static int binarySearchIntIterable(int[] list, int value) {
        assert list != null : "list can't be null";
        int lowerIndex = 0;
        int upperIndex = list.length - 1;
        while (lowerIndex <= upperIndex) {
            int index = lowerIndex + (upperIndex - lowerIndex) / 2;
            //System.out.println(value + " " + index + " " + list[index]);
            if (value == list[index]) {
                return index;
            } else if (value < list[index]) {
                upperIndex = index - 1;
            } else {
                lowerIndex = index + 1;
            }
        }
        return -(lowerIndex + 1);
    }
}