package tree.binary;

import utils.Logger;

/**
 * Created by Chernyshov Yuriy on 03.09.13
 */
public class CommonNodeFinder {

    private BinaryTreeEntity<Integer> firstNode;
    private BinaryTreeEntity<Integer> secondNode;
    private BinaryTreeEntity<Integer> commonNode;

    public CommonNodeFinder() {

    }

    public void setFirstNode(BinaryTreeEntity<Integer> firstNode) {
        this.firstNode = firstNode;
    }

    public void setSecondNode(BinaryTreeEntity<Integer> secondNode) {
        this.secondNode = secondNode;
    }

    public void printCommonNode() {
        Logger.printMessage("Common node value : " + commonNode);
    }

    public void computeCommonNodeRecursively(BinaryTreeEntity<Integer> currentNode) {
        if (currentNode == null) {
            return;
        }
        Logger.printMessage("Binary Tree entity value " + currentNode.getEntityValue());
        if (currentNode.getEntityValue().equals(firstNode.getEntityValue()) ||
                currentNode.getEntityValue().equals(secondNode.getEntityValue())) {
            commonNode = currentNode;
            return;
        }
        if (firstNode.getEntityValue() < currentNode.getEntityValue() ||
                secondNode.getEntityValue() < currentNode.getEntityValue()) {
            if (firstNode.getEntityValue() > currentNode.getEntityValue() ||
                    secondNode.getEntityValue() > currentNode.getEntityValue()) {
                        commonNode = currentNode;
                        return;
                    }
        }
        computeCommonNodeRecursively(currentNode.getLeft());
        computeCommonNodeRecursively(currentNode.getRight());
    }
}