package tree.binary;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 16.05.13
 * Time: 17:20
 */
public class BinaryTreeTest extends TestCase {

    private BinaryTree<Integer> binaryTree;
    private Integer [] data = { 5, 3, 9, 1, 4, 6, };

    @Before
    public void setUp() throws Exception {
        System.out.println("Set Up test");
        binaryTree = new BinaryTree<Integer>();
        binaryTree.insert(data);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Tear Down test");
    }

    @Test
    public void testGetRoot() throws Exception {
        BinaryTreeEntity<Integer> expected = new BinaryTreeEntity<Integer>(data[0]);
        assertEquals(binaryTree.getRoot(), expected);
    }

    @Test
    public void testContains() throws Exception {
        for (int value : data) {
            assertTrue(binaryTree.contains(value));
            assertFalse(binaryTree.contains(value * 1000));
        }
    }

    @Test
    public void testInsertList() throws Exception {
        // When you insert a list, you get it back without
        // alteration using a pre-order, depth-first traversal.
        List<String> data = Arrays.asList("F", "B", "A", "D", "C", "E", "G", "I", "H");
        BinaryTree<String> tree = new BinaryTree<String>();
        tree.insert(data);

        // Check the size
        assertEquals(tree.size(), data.size());

        // Now check the values
        //BinaryTreeIterator<String> iterator = new DepthFirstIterator<String>(tree);
        //int i = 0;
        //while (iterator.hasNext()) {
        //    assertEquals("i = " + i, iterator.next(), data.get(i++));
        //}
    }

    @Test
    public void testInsertArray() {
    // When you insert a list, you get it back without
    // alteration using a pre-order, depth-first traversal.
        String [] data = {"F","B","A","D","C","getEdgesNumber","G","I","H",};
        BinaryTree<String> tree
                = new BinaryTree<String>();
        tree.insert(data);

        assertEquals(tree.size(), data.length);

        /*BinaryTreeIterator<String> iterator = new DepthFirstIterator<String>(tree);
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals("i = " + i, iterator.next(),  data[i++]);
        }*/
    }

    @Test
    public void testInsertNullList() {
        List<String> data = null;
        BinaryTree<String> tree = new BinaryTree<String>();
        tree.insert(data);
        assertEquals(tree.size(), 0);
    }

    @Test
    public void testInsertNullArray() {
        String [] data = null;
        BinaryTree<String> tree = new BinaryTree<String>();
        tree.insert(data);
        assertEquals(tree.size(), 0);
    }

    @Test
    public void testSize() throws Exception {
        assertEquals(binaryTree.size(), data.length);
    }

    @Test
    public void testHeight() throws Exception {
        int expected = 3;
        assertEquals(binaryTree.height(), expected);
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertFalse(binaryTree.isEmpty());
        assertTrue(new BinaryTree<Integer>().isEmpty());
    }
}