package tree.binary;

import java.util.Stack;

/**
 * Created by ChernyshovYuriy on 9/23/13
 */
public class BST_2sumFinder {

    public boolean hasTwoNodes(BinaryTreeEntity pRoot, int sum) {
        Stack<BinaryTreeEntity> nextNodes = new Stack<BinaryTreeEntity>();
        Stack<BinaryTreeEntity> prevNodes = new Stack<BinaryTreeEntity>();
        buildNextNodes(pRoot, nextNodes);
        buildPrevNodes(pRoot, prevNodes);

        BinaryTreeEntity<Integer> pNext = getNextNode(nextNodes);
        BinaryTreeEntity<Integer> pPrev = getPreviousNode(prevNodes);
        while (pNext != null && pPrev != null && pNext != pPrev) {
            int currentSum = pNext.getEntityValue() + pPrev.getEntityValue();
            if (currentSum == sum)
                return true;
            if (currentSum < sum)
                pNext = getNextNode(nextNodes);
            else
                pPrev = getPreviousNode(prevNodes);
        }

        return false;
    }

    void buildNextNodes(BinaryTreeEntity pRoot, Stack<BinaryTreeEntity> nodes) {
        BinaryTreeEntity pNode = pRoot;
        while (pNode != null) {
            nodes.push(pNode);
            pNode = pNode.getLeft();
        }
    }

    void buildPrevNodes(BinaryTreeEntity pRoot, Stack<BinaryTreeEntity> nodes) {
        BinaryTreeEntity pNode = pRoot;
        while (pNode != null) {
            nodes.push(pNode);
            pNode = pNode.getRight();
        }
    }

    private BinaryTreeEntity getNextNode(Stack<BinaryTreeEntity> nodes) {
        BinaryTreeEntity pNext = null;
        if (!nodes.empty()) {
            pNext = nodes.pop();
            BinaryTreeEntity pRight = pNext.getRight();
            while (pRight != null) {
                nodes.push(pRight);
                pRight = pRight.getLeft();
            }
        }
        return pNext;
    }

    private BinaryTreeEntity getPreviousNode(Stack<BinaryTreeEntity> nodes) {
        BinaryTreeEntity pPrev = null;
        if (!nodes.empty()) {
            pPrev = nodes.pop();
            BinaryTreeEntity pLeft = pPrev.getLeft();
            while (pLeft != null) {
                nodes.push(pLeft);
                pLeft = pLeft.getRight();
            }
        }
        return pPrev;
    }
}