package tree.binary;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Chernyshov Yuriy
 * Date: 16.05.13
 * Time: 13:10
 */
public class BinaryTree<T extends Comparable<T>> {

    private BinaryTreeEntity<T> root;

    public BinaryTree() {
        this(null);
    }

    public BinaryTree(BinaryTreeEntity<T> root) {
        this.root = root;
    }

    public BinaryTreeEntity<T> getRoot() {
        return root;
    }

    public boolean contains(T value) {
        return contains(root, value);
    }

    private boolean contains(BinaryTreeEntity<T> node, T value) {
        // empty tree can't contain the value OR value cannot be null
        if ((node == null) || (value == null)) {
            return false;
        }
        if (value.equals(node.getEntityValue())) {
            return true;
        } else if (value.compareTo(node.getEntityValue()) < 0) {
            return contains(node.getLeft(), value);
        } else {
            return contains(node.getRight(), value);
        }
    }

    public void insert(T value) {
        root = insert(root, value);
    }

    public void insert(List<T> values) {
        if ((values != null) && (values.size() > 0)) {
            for (T value : values) {
                insert(value);
            }
        }
    }

    public void insert(T [] values) {
        if ((values != null) && (values.length > 0)) {
            insert(Arrays.asList(values));
        }
    }

    private BinaryTreeEntity<T> insert(BinaryTreeEntity<T> node, T value) {
        if (node == null) {
            return new BinaryTreeEntity<T>(value);
        } else {
            if (value.compareTo(node.getEntityValue()) < 0) {
                node.setLeft(insert(node.getLeft(), value));
            } else {
                node.setRight(insert(node.getRight(), value));
            }
        }
        return node;
    }

    public int size() {
        return size(root);
    }

    private int size(BinaryTreeEntity<T> node) {
        if (node == null) {
            return 0;
        } else {
            return (size(node.getLeft()) + 1 + size(node.getRight()));
        }
    }

    public int height() {
        return height(root);
    }

    public int height(BinaryTreeEntity<T> node) {
        if (node == null) {
            return 0;
        } else {
            int leftHeight = height(node.getLeft());
            int rightHeight = height(node.getRight());
            return (Math.max(leftHeight, rightHeight) + 1);
        }
    }

    public boolean isEmpty() {
        return (root == null);
    }
}