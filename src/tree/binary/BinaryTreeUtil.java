package tree.binary;

import utils.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/16/13
 * Time: 9:48 PM
 */
public class BinaryTreeUtil {

    public static void traverseBinaryTreeRecursively(BinaryTreeEntity binaryTreeEntity,
                                                     BinaryTreeTraverseOrder.Order traverseOrder, int depthCounter) {
        if (binaryTreeEntity == null) {
            depthCounter -= 1;
            return;
        }
        if (traverseOrder == BinaryTreeTraverseOrder.Order.PRE_ORDER) {
            Logger.printMessage("Binary Tree entity value " + binaryTreeEntity.getEntityValue());
        }
        traverseBinaryTreeRecursively(binaryTreeEntity.getLeft(), traverseOrder, depthCounter + 1);
        if (traverseOrder == BinaryTreeTraverseOrder.Order.IN_ORDER) {
            Logger.printMessage("Binary Tree entity value " + binaryTreeEntity.getEntityValue() + ", depth " +
                    depthCounter);
        }
        traverseBinaryTreeRecursively(binaryTreeEntity.getRight(), traverseOrder, depthCounter + 1);
        if (traverseOrder == BinaryTreeTraverseOrder.Order.POST_ORDER) {
            Logger.printMessage("Binary Tree entity value " + binaryTreeEntity.getEntityValue());
        }
    }

    public static List<BinaryTreeEntity> traverseBinaryTreeIterative(BinaryTreeEntity binaryTreeEntity,
                                                                     BinaryTreeTraverseOrder.Order traverseOrder) {
        return traverseBinaryTreeIterative(binaryTreeEntity, traverseOrder, 0, Integer.MAX_VALUE);
    }

    public static List<BinaryTreeEntity> traverseBinaryTreeIterative(BinaryTreeEntity binaryTreeEntity,
                                                                     BinaryTreeTraverseOrder.Order traverseOrder,
                                                                     int minDepth, int maxDepth) {
        Logger.printMessage("\nTraverse Binary Tree Iterative");
        Stack<BinaryTreeEntity> entityStack = new Stack<BinaryTreeEntity>();
        List<BinaryTreeEntity> binaryTreeEntities = new LinkedList<BinaryTreeEntity>();
        if (traverseOrder == BinaryTreeTraverseOrder.Order.POST_ORDER) {
            // TODO: Does not work !!!
            if (binaryTreeEntity == null) {
                return binaryTreeEntities;
            }
            entityStack.push(binaryTreeEntity);
            BinaryTreeEntity previousEntity = null;
            while (!entityStack.isEmpty()) {
                BinaryTreeEntity currentEntity = entityStack.peek();
                if (previousEntity == null || previousEntity.getLeft() == currentEntity ||
                        previousEntity.getRight() == currentEntity) {
                    if (currentEntity.getLeft() != null) {
                        entityStack.push(currentEntity.getLeft());
                    } else if (currentEntity.getRight() != null) {
                        entityStack.push(currentEntity.getRight());
                    }
                } else if (currentEntity.getLeft() == previousEntity) {
                    if (currentEntity.getRight() != null) {
                        entityStack.push(currentEntity.getRight());
                    }
                } else {
                    Logger.printMessage("Binary Tree entity value " + binaryTreeEntity.getEntityValue() +
                            " depth: " + entityStack.size());
                    entityStack.pop();
                }
                previousEntity = currentEntity;
            }
        } else {
            while (!entityStack.isEmpty() || binaryTreeEntity != null) {
                if (binaryTreeEntity != null) {
                    if (traverseOrder == BinaryTreeTraverseOrder.Order.PRE_ORDER) {
                        Logger.printMessage("Binary Tree entity value " + binaryTreeEntity.getEntityValue() +
                                " depth: " + entityStack.size());
                    }
                    entityStack.push(binaryTreeEntity);
                    binaryTreeEntity = binaryTreeEntity.getLeft();
                } else {
                    binaryTreeEntity = entityStack.pop();
                    if (traverseOrder == BinaryTreeTraverseOrder.Order.IN_ORDER) {
                        if (binaryTreeEntity.getDepth() >= minDepth && binaryTreeEntity.getDepth() <= maxDepth) {
                            Logger.printMessage("Binary Tree entity value " + binaryTreeEntity.getEntityValue() +
                                    " depth: " + binaryTreeEntity.getDepth());
                            binaryTreeEntities.add(binaryTreeEntity);
                        }
                    }
                    binaryTreeEntity = binaryTreeEntity.getRight();
                }
            }
        }
        return binaryTreeEntities;
    }

    public static Integer getMinimum(BinaryTreeEntity binaryTreeEntity) {
        if (binaryTreeEntity.getLeft() == null) {
            Logger.printMessage("Binary Tree minimum " + binaryTreeEntity.getEntityValue());
            return (Integer) binaryTreeEntity.getEntityValue();
        }
        return getMinimum(binaryTreeEntity.getLeft());
    }

    public static Integer getMaximum(BinaryTreeEntity binaryTreeEntity) {
        if (binaryTreeEntity.getRight() == null) {
            Logger.printMessage("Binary Tree maximum " + binaryTreeEntity.getEntityValue());
            return (Integer) binaryTreeEntity.getEntityValue();
        }
        return getMaximum(binaryTreeEntity.getRight());
    }

    public static void setDepth(BinaryTreeEntity rootEntity) {
        if (rootEntity == null) {
            return;
        }
        Stack stack = new Stack();
        rootEntity.setDepth(1);
        stack.push(rootEntity);
        BinaryTreeEntity temp;
        while(stack.size() > 0) {
            temp = (BinaryTreeEntity) stack.pop();
            if (temp != null) {
                if (temp.getLeft() != null) {
                    temp.getLeft().setDepth(temp.getDepth() + 1);
                    stack.push(temp.getLeft());
                }
                if (temp.getRight() != null) {
                    temp.getRight().setDepth(temp.getDepth() + 1);
                    stack.push(temp.getRight());
                }
            }
        }
    }
}