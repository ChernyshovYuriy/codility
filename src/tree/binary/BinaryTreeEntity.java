package tree.binary;

/**
 * Created with IntelliJ IDEA.
 * User: Chernyshov Yuriy
 * Date: 16.05.13
 * Time: 12:38
 */
public class BinaryTreeEntity<T extends Comparable<T>> implements Comparable<T> {

    BinaryTreeEntity<T> leftEntity;
    BinaryTreeEntity<T> rightEntity;
    private T entityValue;
    private T parent;
    private int depth;

    public BinaryTreeEntity(T value) {
        this(value, null, null);
    }

    public BinaryTreeEntity(T value, BinaryTreeEntity<T> leftEntity, BinaryTreeEntity<T> rightEntity) {
        this.setEntityValue(value);
        this.leftEntity = leftEntity;
        this.rightEntity = rightEntity;
    }

    public T getParent() {
        return parent;
    }

    public void setParent(T parent) {
        this.parent = parent;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public T getEntityValue() {
        return entityValue;
    }

    public void setEntityValue(T entityValue) {
        if (entityValue == null) {
            throw new IllegalArgumentException("Entity Value cannot be null");
        }
        this.entityValue = entityValue;
    }

    public BinaryTreeEntity<T> getLeft() {
        return leftEntity;
    }

    public void setLeft(BinaryTreeEntity<T> value) {
        leftEntity = value;
    }

    public BinaryTreeEntity<T> getRight() {
        return rightEntity;
    }

    public void setRight(BinaryTreeEntity<T> right) {
        this.rightEntity = right;
    }

    public boolean isLeaf() {
        return ((leftEntity == null) && (rightEntity == null));
    }

    @Override
    public int compareTo(T object) {
        BinaryTreeEntity<T> other = (BinaryTreeEntity<T>) object;
        return getEntityValue().compareTo(other.getEntityValue());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        BinaryTreeEntity node = (BinaryTreeEntity) object;
        return entityValue.equals(node.entityValue);
    }

    @Override
    public int hashCode() {
        return entityValue.hashCode();
    }

    @Override
    public String toString() {
        return entityValue.toString();
    }
}