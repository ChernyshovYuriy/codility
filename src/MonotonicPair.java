/**
 * Created by ChernyshovYuriy on 4/26/14
 */
public class MonotonicPair {

    public int solution(int[] data) {
        int result = 0;
        int dataLength = data.length;
        if (dataLength < 1 || dataLength > 300000) {
            return result;
        }
        int[] top = new int[dataLength];
        int max = Integer.MIN_VALUE;
        for (int i = dataLength - 1; i >= 0; i--) {
            if (data[i] > max) {
                max = data[i];
            }
            top[i] = max;
        }
        for (int i = 0; i < dataLength; i++) {
            int c = binarySearch(top, data[i]) - i;
            if (c > result) {
                result = c;
            }
            if (result >= dataLength - i) {
                return result;
            }
        }
        return result;
    }

    private int binarySearch(int[] t, int min) {
        int s = 0;
        int e = t.length - 1;
        if (t[e] >= min) {
            return e;
        }
        while (true) {
            int x = (s + e) / 2;
            if (x == t.length - 1) {
                return t.length - 1;
            }
            if (t[x] >= min && t[x + 1] < min) {
                return x;
            }
            if (t[x] < min) {
                e = x;
            } else {
                s = x;
            }
        }
    }
}