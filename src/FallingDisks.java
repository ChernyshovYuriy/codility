/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 10:48 PM
 */
public class FallingDisks {

    public static int falling_disks(int[] A, int[] B) {
        int minimum = A[0];
        int nbDisk = 0;

        for (int i = 0; i < A.length; i++) {
            if (A[i] < minimum) {
                minimum = A[i];
            }
            if (A[i] > minimum) {
                A[i] = minimum;
            }
        }

        for (int i = A.length - 1; i >= 0; i--) {
            if (B[nbDisk] <= A[i]) {
                nbDisk++;
            }
            if (nbDisk == B.length) {
                break;
            }
        }
        System.out.println("FallingDisks number " + nbDisk);
        return nbDisk;
    }

}