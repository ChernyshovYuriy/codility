/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 11:23 PM
 */
public class BinaryPeriod {

    public static int binaryPeriod(int N) {
        String binaryString = Integer.toBinaryString(N);
        int size;
        String str1;
        String str2;
        int counter = -1;
        System.out.println("Binary String for " + N + " is " + binaryString);
        if (binaryString.length() % 2 == 0) {
            for (int i = 0; i < binaryString.length(); i++)  {
                size = binaryString.length() >> 1;
                str1 = binaryString.substring(0, size);
                str2 = binaryString.substring(size, binaryString.length());
                if (str1.equals(str2)) {
                    counter = str1.length();
                    binaryString = binaryString.substring(0, size);
                    //System.out.println("- " + counter);
                } else {
                    binaryString = binaryString.substring(0, binaryString.length() - 2);
                }
            }
        }
        System.out.println("Binary Period for " + N + " is " + counter);
        return counter;
    }
}