package datastructure;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 5/2/14
 * Time: 3:08 PM
 */
public class PrefixTree {

    public PrefixTreeNode root = new PrefixTreeNode();

    public void insertString(String value) {
        PrefixTreeNode node = root;
        for (char c : value.toCharArray()) {
            if (!node.children.containsKey(c)) {
                 node.children.put(c, new PrefixTreeNode());
            }
            node = node.children.get(c);
        }
        node.isLeaf = true;
    }

    public void printSorted(PrefixTreeNode node, String string) {
        for (char c : node.children.keySet()) {
            Logger.printMessage("  " + c + " " + (string + c) + " " + node.children.size());
            printSorted(node.children.get(c), string + c);
        }
        if (node.isLeaf) {
            Logger.printMessage(string);
        }
    }
}