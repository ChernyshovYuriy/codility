package datastructure;

import java.util.HashSet;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 5/9/14
 * Time: 9:23 AM
 */
public class PrefixSuffixSet {

    public int solution(int[] A) {
        HashSet<Integer> leftItems = new HashSet<Integer>();
        HashSet<Integer> rightItems = new HashSet<Integer>();
        HashSet<Integer> differentItems = new HashSet<Integer>();
        int n = A.length, count = 0;
        int i = 0, j = n - 1;
        int[] pointers = new int[2];
        while (i < n && j >= 0) {
            search(A, i, leftItems, 1, pointers, 0);
            search(A, j, rightItems, -1, pointers, 1);
            compare(A[i], rightItems, differentItems);
            compare(A[j], leftItems, differentItems);
            if (differentItems.size() == 0) {
                count += (pointers[0] - i + 1) * (j - pointers[1] + 1);
                if (count > 1000000000)
                    return 1000000000;
            }
            i = pointers[0] + 1;
            j = pointers[1] - 1;
        }
        return count;
    }

    private void compare(int v, HashSet<Integer> dst, HashSet<Integer> differences) {
        if (!dst.contains(v)) {
            if (!differences.contains(v))
                differences.add(v);
        } else if (differences.contains(v))
            differences.remove(v);
    }

    private void search(int[] A, int i, HashSet<Integer> dataItems, int e, int[] pointers, int pointerIndex) {
        int n = A.length;
        dataItems.add(A[i]);
        pointers[pointerIndex] = i + e;
        while (pointers[pointerIndex] >= 0 && pointers[pointerIndex] < n) {
            if (!dataItems.contains(A[pointers[pointerIndex]]))
                break;
            pointers[pointerIndex] += e;
        }
        pointers[pointerIndex] -= e;
    }
}