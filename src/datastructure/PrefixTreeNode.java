package datastructure;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 5/2/14
 * Time: 3:09 PM
 */
public class PrefixTreeNode {

    public Map<Character, PrefixTreeNode> children = new TreeMap<Character, PrefixTreeNode>();
    public boolean isLeaf;
}