import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 7/5/13
 * Time: 7:09 PM
 */
public class IntersectionDiscsPairs {

    public static int getIntersectionDiscsPairs(int[] arrayA) {
        if (arrayA == null) {
            return 0;
        }
        int arraySize = arrayA.length;
        if (arraySize < 2) {
            return 0;
        }
        int[] arrayB = Arrays.copyOf(arrayA, arraySize);
        Arrays.sort(arrayB);
        int biggest = arrayB[arraySize - 1];
        int intersections = 0;
        for (int i = 0; i < arraySize; i++) {
            for (int j = i + 1; j < arraySize; j++) {
                if (j - biggest > i + arrayA[i]) {
                    break;
                }
                if (j - arrayA[j] <= i + arrayA[i]) {
                    intersections++;
                }
                if (intersections > 10000000) {
                    return -1;
                }
            }
        }
        return intersections;
    }

}