package algorithms;

import utils.Logger;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/16/13
 * Time: 3:03 PM
 */
public class Codility {

    private int absDistinct(int[] data) {
        int dataLength = data.length;
        if (dataLength < 1 || dataLength > 100000) {
            return 0;
        }
        HashSet<Integer> holder = new HashSet<Integer>();
        for (int i : data) {
            if (i < -2147483647 || i > 2147483647) {
                continue;
            }
            if (i < 0) {
                i = -i;
            }
            if (holder.contains(i)) {
                continue;
            }
            holder.add(i);
        }
        return holder.size();
    }

    public static int binaryGap(int inputN) {
        if (inputN > 2147483647) {
            return 0;
        }
        String binary = Integer.toBinaryString(inputN);
        int counter = 0;
        int maxCounter = Integer.MIN_VALUE;
        boolean startCount = false;
        for (int i = 0; i < binary.length(); i++) {
            // 49 = 1
            // 48 = 0
            if (binary.codePointAt(i) == 49) {
                startCount = true;
                if (counter > maxCounter) {
                     maxCounter = counter;
                }
                counter = 0;
            }
            if (startCount && binary.codePointAt(i) == 48) {
                counter++;
            }
        }
        return maxCounter;
    }

    public static int equilibrium(int[] data) {
        int dataLength = data.length;
        if (dataLength < 0 || dataLength > 1000000) {
            return -1;
        }
        long sum = 0;
        int i;
        for (i = 0; i < dataLength; i++) {
            sum += (long) data[i];
        }
        long sum_left = 0;
        long sum_right;
        for (i = 0; i < dataLength; i++) {
            sum_right = sum - sum_left - (long) data[i];
            if (sum_left == sum_right) {
                return i;
            }
            sum_left += (long) data[i];
        }
        return -1;
    }

    public static int dominator(int[] data) {
        int dataSize = data.length;
        int dominator = -1;
        if (dataSize < 1 && dataSize > 1000000) {
            return dominator;
        }
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        Integer counter;
        int maxCounter = Integer.MIN_VALUE;
        int maxCounterKey = -1;
        int maxCounterIndex = 0;
        int i = 0;
        for (int item : data) {
            if (item < 0 && item > 2147483647) {
                continue;
            }
            counter = map.get(item);
            if (counter == null) {
                counter = 0;
            }
            counter++;
            map.put(item, counter);
            if (counter > maxCounter) {
                maxCounter = counter;
                maxCounterKey = item;
                maxCounterIndex = i;
            }
            i++;
        }
        if (maxCounterKey != -1 && map.get(maxCounterKey) > dataSize >> 1) {
            dominator = maxCounterIndex;
        }
        return dominator;
    }

    public static int maxProfit(int[] data) {
        int result = 0;
        int dataLength = data.length;
        if (dataLength < 1 || dataLength > 1000000) {
            return result;
        }
        int min = data[0];
        int maxProfit = 0;
        for (int i = 0; i < dataLength - 1; i++) {
            int first = data[i];
            int next = data[i + 1];

            if (first > -1 && first < min) {
                min = first;
            }

            int diff = next - min;
            if (diff > 0 && maxProfit < diff) {
                maxProfit = diff;
            }
        }
        return maxProfit;
    }

    public static void treeHeightSolution() {
        Tree node_A = new Tree();
        Tree node_B = new Tree();
        Tree node_C = new Tree();
        Tree node_D = new Tree();
        Tree node_E = new Tree();
        Tree node_F = new Tree();

        node_A.l = node_B;
        node_A.r = node_C;
        node_B.l = node_D;
        node_B.r = node_E;
        node_C.l = node_F;

        Logger.printMessage("Tree height: " + (treeHeight(node_A) - 1));
    }

    public static int treeHeight(Tree tree) {
        if (tree == null) {
            return 0;
        } else {
            int leftHeight = treeHeight(tree.l);
            int rightHeight = treeHeight(tree.r);
            return (Math.max(leftHeight, rightHeight) + 1);
        }
    }

    public static int firstCoveringPrefix(int[] data) {
        int result = 0;
        int dataLength = data.length;
        if (dataLength < 0 || dataLength > 1000000) {
            return result;
        }
        boolean[] occurs = new boolean[dataLength];
        for (int i = 0; i < dataLength; i++) {
            if (!occurs[data[i]]) {
                occurs[data[i]] = true;
                result = i;
            }
        }
        return result;
    }

    // http://stackoverflow.com/questions/337664/counting-inversions-in-an-array
    public static int inversion(int[] data) {
        int result = 0;
        int dataLength = data.length;
        if (dataLength < 0 || dataLength > 100000) {
            return result;
        }

        if (dataLength < 2)
            return 0;

        int m = (dataLength + 1) / 2;
        int left[] = Arrays.copyOfRange(data, 0, m);
        int right[] = Arrays.copyOfRange(data, m, dataLength);

        return inversion(left) + inversion(right) + merge(data, left, right);
    }

    private static int merge(int[] arr, int[] left, int[] right) {
        int i = 0, j = 0, count = 0;
        while (i < left.length || j < right.length) {
            if (i == left.length) {
                arr[i+j] = right[j];
                j++;
            } else if (j == right.length) {
                arr[i+j] = left[i];
                i++;
            } else if (left[i] <= right[j]) {
                arr[i+j] = left[i];
                i++;
            } else {
                arr[i+j] = right[j];
                count += left.length-i;
                j++;
            }
        }
        return count;
    }

    public static int stoneWall(int[] data) {
        int[] stack = new int[data.length];
        int stack_num = 0;
        int stones = 0;
        for (int i : data) {
            Logger.printMessage("\nBlock: " + i);
            //Logger.printMessage("   stack_num: " + stack_num + ", stack[stack_num - 1]: " + (stack_num < 1 ? 0 : stack[stack_num - 1]));
            while (stack_num > 0 && stack[stack_num - 1] > i) {
                stack_num--;
                Logger.printMessage("      clear stack, stack_num: " + stack_num);
            }
            //Logger.printMessage("   stack_num: " + stack_num + ", stack[stack_num - 1]: " + (stack_num < 1 ? 0 : stack[stack_num - 1]));
            if (!(stack_num > 0 && stack[stack_num - 1] == i)) {
                stack[stack_num] = i;
                stones++;
                stack_num++;
                Logger.printMessage("      fill stack with: " + i);
            }
        }
        return stones;
    }

    public static int maximumProduct(String s) {
        int result = 0;
        int stringLength = s.length();
        if (stringLength > 300000) {
            return result;
        }

        int matchesNumber;
        int product = 0;

        /**
         * Version A
         */

        char[] textChars = s.toCharArray();
        for (int i = stringLength - 1; i >= 2; i--) {
            matchesNumber = BoyerMoor.indexOf(textChars, s.substring(0, i).toCharArray());
            if (matchesNumber != -1) {
                product = i * matchesNumber;
                //Logger.printMessage("L:" + i + ", N:" + matchesNumber + ", P:" + product);
            }
            if (product > result) {
                result = product;
            }
        }

        /**
         * Version B
         */

        /*

        int[] ints = new int[stringLength];
        char[] chars = new char[stringLength];
        for (int i = 0; i < stringLength; i++) {
            ints[i] = s.codePointAt(i);
            chars[i] = s.charAt(i);
        }
        boolean isMatch;
        int prefixLength = 1;
        while (prefixLength < stringLength) {
            prefixLength++;
            matchesNumber = 0;
            for (int i = 0; i < stringLength; i++) {
                if (i + prefixLength > stringLength) {
                    break;
                }
                isMatch = true;
                for (int j = 0; j < prefixLength; j++) {
                    if (ints[i + j] != ints[j]) {
                        isMatch = false;
                        break;
                    }
                }
                if (isMatch) {
                    matchesNumber++;
                }
            }
            product = prefixLength * matchesNumber;
            if (product > result) {
                result = product;
            }
        }*/

        if (result == 0 || result < stringLength) {
            result = stringLength;
        }
        if (result > 1000000000) {
            result = 1000000000;
        }
        return result;
    }

    public int getMaxFlags(int[] data) {
        if (data.length <= 2 ) {
            return 0;
        }

        // Threshold - is the minimum distance between peaks satisfying the invariant when the
        // distance between peaks must be greater or equal to the number of peaks

        int threshold = (int)Math.ceil(Math.sqrt(data.length));

        if (threshold * (threshold - 1) > data.length) {
            threshold--;
        }

        int prevIndex = -1;
        int flags = 0;

        while(true) {
            // Skip first an the last elements of the A
            for (int i = 1; i < data.length - 1; i++) {
                // Determine if current element is a peak
                if (!(data[i] > data[i - 1] && data[i] > data[i + 1])) {
                    continue;
                }

                // If current element is the first peak - just add first flag and save current peak as previous
                if(prevIndex < 0) {
                    prevIndex = i;
                    flags++;
                    continue;
                }

                // If the distance between current and previous peaks is less then threshold then do not add flag
                if (i - prevIndex < threshold) {
                    continue;
                }

                // If the distance between current and previous peaks is greater then threshold then add flag and make
                // current peak as previous
                flags++;
                prevIndex = i;
            }

            // If number of found flags is greater or equal to current threshold then solution is found
            if (flags >= threshold) {
                if (flags > threshold) {
                    flags = threshold;
                }
                break;
            }

            // The number of found flags is less then threshold, so decrease threshold and count flags again
            threshold--;
            prevIndex = -1;
            flags = 0;
        }

        return flags;
    }
}

class Tree {
    public int x;
    public Tree l;
    public Tree r;
}