package algorithms;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 10/10/13
 * Time: 10:30 PM
 */
public class TourManager {

    // Holds our cities
    private static ArrayList<City> destinationCities = new ArrayList<City>();

    // Adds a destination city
    public static void addCity(City city) {
        destinationCities.add(city);
    }

    // Get a city
    public static City getCity(int index) {
        return destinationCities.get(index);
    }

    // Get the number of destination cities
    public static int numberOfCities() {
        return destinationCities.size();
    }
}