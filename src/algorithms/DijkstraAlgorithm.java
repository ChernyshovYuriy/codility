package algorithms;

import graphs.Edge;
import graphs.Graph;
import graphs.Vertex;

import java.util.*;

/**
 * Created by Chernyshov Yuriy on 8/8/13
 */
public class DijkstraAlgorithm {

    private final List<Vertex> vertexes;
    private final List<Edge> edges;
    private Set<Vertex> settledVertexes;
    private Set<Vertex> unsettledVertexes;
    private Map<Vertex, Vertex> predecessors;
    private Map<Vertex, Integer> distance;

    public DijkstraAlgorithm(Graph graph) {
        // Create a copy of the array so that we can operate on this array
        vertexes = new ArrayList<Vertex>(graph.getVertexes());
        edges = new ArrayList<Edge>(graph.getEdges());
    }

    public void execute(Vertex sourceVertex) {
        if (settledVertexes != null) {
            settledVertexes.clear();
        }
        settledVertexes = new HashSet<Vertex>();

        if (unsettledVertexes != null) {
            unsettledVertexes.clear();
        }
        unsettledVertexes = new HashSet<Vertex>();
        unsettledVertexes.add(sourceVertex);

        if (distance != null) {
            distance.clear();
        }
        distance = new HashMap<Vertex, Integer>();
        distance.put(sourceVertex, 0);

        if (predecessors != null) {
            predecessors.clear();
        }
        predecessors = new HashMap<Vertex, Vertex>();

        Vertex vertex;
        while (unsettledVertexes.size() > 0) {
            vertex = getMinimum(unsettledVertexes);
            settledVertexes.add(vertex);
            unsettledVertexes.remove(vertex);
            findMinimalDistances(vertex);
        }
    }

    public Vertex getVertexAt(int position) {
        return vertexes.get(position);
    }

    private void findMinimalDistances(Vertex vertex) {
        List<Vertex> adjacentVertexes = getNeighbors(vertex);
        int nextDistance;
        for (Vertex target : adjacentVertexes) {
            nextDistance = getShortestDistance(vertex) + getDistance(vertex, target);
            if (getShortestDistance(target) > nextDistance) {
                distance.put(target, nextDistance);
                predecessors.put(target, vertex);
                unsettledVertexes.add(target);
            }
        }
    }

    public int getDistance(Vertex node, Vertex target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(node) && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private List<Vertex> getNeighbors(Vertex node) {
        List<Vertex> neighbors = new ArrayList<Vertex>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(node) && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    private Vertex getMinimum(Set<Vertex> vertexes) {
        Vertex minimum = null;
        for (Vertex vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(Vertex vertex) {
        return settledVertexes.contains(vertex);
    }

    private int getShortestDistance(Vertex destination) {
        Integer distanceValue = distance.get(destination);
        if (distanceValue == null) {
            return Integer.MAX_VALUE;
        } else {
            return distanceValue;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<Vertex> getPath(Vertex target) {
        LinkedList<Vertex> path = new LinkedList<Vertex>();
        Vertex step = target;
        // Check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }
}