package algorithms;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utils.Logger;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/10/13
 * Time: 1:32 PM
 */
public class MaxFlagsNumberTest extends TestCase {

    private MaxFlagsNumber maxFlagsNumber;

    public MaxFlagsNumberTest() {
        maxFlagsNumber = new MaxFlagsNumber();
    }

    @Before
    public void setUp() throws Exception {
        //Logger.printMessage(getClass().getSimpleName() + " before test");
    }

    @After
    public void tearDown() throws Exception {
        //Logger.printMessage(getClass().getSimpleName() + " after test");
    }

    @Test
    public void testExampleData() {
        Logger.printMessage(" - Example data test begin ...");
        int[] data = new int[]{1,5,3,4,3,4,1,2,3,4,6,2};// Res = 3
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Example data test result: " + result);
        assertEquals(3, result);
        Logger.printMessage(" - Example data test passed");
    }

    @Test
    public void testExtremeMinimumData1() {
        Logger.printMessage(" - Extreme minimum data 1 test begin ...");
        int[] data = new int[]{1};
        int result = maxFlagsNumber.execute(data);
        assertEquals(0, result);
        Logger.printMessage(" - Extreme minimum data 1 test passed");
    }

    @Test
    public void testExtremeMinimumData2() {
        Logger.printMessage(" - Extreme minimum data 2 test begin ...");
        int[] data = new int[]{};
        int result = maxFlagsNumber.execute(data);
        assertEquals(0, result);
        Logger.printMessage(" - Extreme minimum data 2 test passed");
    }

    @Test
    public void testTreeElementsData() {
        Logger.printMessage(" - Tree elements data test begin ...");
        int[] data = new int[]{1,2,1};
        int result = maxFlagsNumber.execute(data);
        assertEquals(1, result);
        Logger.printMessage(" - Tree elements data test passed");
    }

    @Test
    public void testWithoutPeaksData() {
        Logger.printMessage(" - Without peaks data test begin ...");
        int[] data = new int[]{1,1,1,1,1,1,2,2};
        int result = maxFlagsNumber.execute(data);
        assertEquals(0, result);
        Logger.printMessage(" - Without peaks data test passed");
    }

    @Test
    public void testSimpleData1() {
        Logger.printMessage(" - Simple data 1 test begin ...");
        int[] data = new int[]{4, 5, 8, 5, 1, 4, 6, 8, 7, 2, 2, 5};  // Res = 2
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 1 test result: " + result);
        assertEquals(2, result);
        Logger.printMessage(" - Simple data 1 test passed");
    }

    @Test
    public void testSimpleData2() {
        Logger.printMessage(" - Simple data 2 test begin ...");
        int[] data = new int[]{7, 10, 4, 5, 7, 4, 6, 1, 4, 3, 3, 7}; // Res = 3
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 2 test result: " + result);
        assertEquals(3, result);
        Logger.printMessage(" - Simple data 2 test passed");
    }

    @Test
    public void testSimpleData3() {
        Logger.printMessage(" - Simple data 3 test begin");
        int[] data = new int[]{1, 4, 2, 5, 1, 2, 7, 6, 1, 2, 1, 1};  // Res = 3
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 3 test result: " + result);
        assertEquals(3, result);
        Logger.printMessage(" - Simple data 3 test passed");
    }

    @Test
    public void testSimpleData4() {
        Logger.printMessage(" - Simple data 4 test begin ...");
        int[] data = new int[]{5, 9, 6, 2, 2, 7, 3, 2, 7, 4, 7, 9};  // Res = 3
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 4 test result: " + result);
        assertEquals(3, result);
        Logger.printMessage(" - Simple data 4 test passed");
    }

    @Test
    public void testSimpleData5() {
        Logger.printMessage(" - Simple data 5 test begin ...");
        int[] data = new int[]{7, 1, 9, 5, 8, 6, 6, 2, 5, 4, 10, 4};  // Res = 2
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 5 test result: " + result);
        assertEquals(2, result);
        Logger.printMessage(" - Simple data 5 test passed");
    }

    @Test
    public void testSimpleData6() {
        Logger.printMessage(" - Simple data 6 test begin ...");
        int[] data = new int[]{0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0, 8, 0};
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 6 test result: " + result);
        assertEquals(4, result);
        Logger.printMessage(" - Simple data 6 test passed");
    }

    @Test
    public void testSimpleData7() {
        Logger.printMessage(" - Simple data 7 test begin ...");
        int[] data = new int[]{9, 9, 4, 3, 5, 4, 5, 2, 8, 9, 3, 1}; // Res = 2
        int result = maxFlagsNumber.execute(data);
        Logger.printMessage(" - Simple data 7 test result: " + result);
        assertEquals(2, result);
        Logger.printMessage(" - Simple data 7 test passed");
    }

    /*@Test
    public void testMediumLengthData() {
        int N = 100000;
        int[] data = new int[N];
        for (int i = 0; i < N; i++) {
            data[i] = randInt(0, N);
        }
        try {
            int result = maxFlagsNumber.execute(data);
        } catch (Throwable throwable) {
            Logger.printError(" - Medium length data test fail");
        }
        Logger.printMessage(" - Medium length data test passed");
    }*/

    private int randInt(int min, int max) {
        // Usually this can be a field rather than a method variable
        Random rand = new Random();
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
}