package algorithms;

import graphs.Edge;
import graphs.Graph;
import graphs.Vertex;
import utils.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Chernyshov Yuriy on 16.09.13
 */
public class Clustering {

    private Graph graph;
    private List<FindUnion> unions;

    public Clustering() {
        unions = new LinkedList<>();
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public void init() {
        for (Vertex vertex : graph.getVertexes()) {
            FindUnion union = new FindUnion(1);
            union.union(vertex.getIntegerId(), vertex.getIntegerId());
            Logger.printMessage("VertexId: " + vertex.getIntegerId() + ", Union: " + union.find(vertex.getIntegerId()));
            unions.add(union);
        }
        Logger.printMessage("Unions: " + unions.size());
    }

    public void maxSpacingClustering(int clusteringNumber) {
        Vertex vertex_A;
        Vertex vertex_B;
        FindUnion union_A;
        FindUnion union_B;
        boolean isConnected;
        int counter = 1;
        //while (findUnion.count() >= clusteringNumber) {
        //while (counter-- >= 0) {
            Logger.printMessage(" --- ");
            for (Edge edge : graph.getEdges()) {
                vertex_A = edge.getSource();
                vertex_B = edge.getDestination();
                union_A = getUnionWithVertex(vertex_A);
                union_B = getUnionWithVertex(vertex_B);
                if (union_A != null && union_B != null) {
                    Logger.printMessage("Union A: " + union_A + " Union_B: " + union_B);
                }
            }
        //}
    }

    private FindUnion getUnionWithVertex(Vertex vertex) {
        for (FindUnion union : unions) {
            //Logger.printMessage("Find: " + union.find(vertex.getIntegerId()));
            if (union.find(vertex.getIntegerId()) != -1) {
                return union;
            }
        }
        return null;
    }

    private boolean hasVertexInUnion(int vertexId) {
        for (FindUnion union : unions) {
            //Logger.printMessage("" + vertexId + " " + union.find(vertexId));
            if (union.find(vertexId) == vertexId) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}