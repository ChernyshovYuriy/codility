package algorithms;

import utils.Logger;

/**
 * Created with Intellij IDEA.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 03.12.14
 * Time: 16:16
 */
public class TimeComplexity {

    public static void main(String[] args) {
        new TimeComplexity();
    }

    /**
     * https://codility.com/media/train/1-TimeComplexity.pdf
     */
    public TimeComplexity() {
        // Problem: You are given an integer n.
        // Count the total of 1 + 2 + . . . + n
        //Logger.printMessage("Result:" + solution(1000));

        int[] A = new int[]{3,1,2,4,3};
        Logger.printMessage("Result:" + tapeEquilibrium(A));
    }

    private double solution(double n) {
        return n * (n + 1) / 2;
    }

    private int tapeEquilibrium(int[] A) {

        long sumLeftToRight = 0;
        long sumRightToLeft;
        long result;
        int size = A.length;

        for (int i = 1; i < size; i++) {
            sumLeftToRight += A[i];
        }

        sumRightToLeft = A[0];
        result = Math.abs(sumRightToLeft - sumLeftToRight);

        long sumDifference;
        for (int i = 1; i < size; i++) {
            sumDifference = Math.abs(sumRightToLeft - sumLeftToRight);
            if (sumDifference < result) {
                result = sumDifference;
            }
            sumRightToLeft += A[i];
            sumLeftToRight -= A[i];
        }
        return (int) result;
    }

    private int frogJump(int X, int Y, int D) {
        if ((Y - X) % D == 0) {
            return (Y - X) / D;
        }
        return (Y - X) / D + 1;
    }

    private int permMissingElem(int[] A) {
        final long N = A.length + 1;
        final long total = (N * (N + 1)) / 2;
        long sum = 0L;
        for (int i : A) {
            sum += i;
        }
        return (int)(total - sum);
    }
}