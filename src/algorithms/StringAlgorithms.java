package algorithms;

import utils.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 1/31/15
 * Time: 11:11 PM
 */
public class StringAlgorithms {

    public static void main(final String[] args) {
        Logger.printMessage("Is substring:" + isSubstring_variant_B("ababab", "ab"));
    }

    private static boolean isSubString_variant_A(String str, String subStr) {
        int indexSubstring = 0;
        int lengthString = (str == null) ? -1 : str.length();
        int lengthSubstring = (subStr == null) ? -1 : subStr.length();
        if (str != null && subStr != null) {
            if (lengthSubstring <= lengthString && lengthSubstring > 0) {
                for (int indexString = 0; indexString < lengthString; indexString++) {
                    if (str.charAt(indexString) == subStr.charAt(indexSubstring)) {
                        if (++indexSubstring == lengthSubstring)
                            break;
                    } else {
                        indexSubstring = 0;
                    }
                }
            }
            return (indexSubstring == lengthSubstring);
        }
        return false;
    }

    private static boolean isSubstring_variant_B(final String inputString, final String subString) {
        int j, jSubString;
        final int stringLength = inputString.length();
        final int subStringLength = subString.length();
        for (int i = 0; i <= stringLength - subStringLength; ++i) {
            for (j = i, jSubString = 0; jSubString < subStringLength; ++j, ++jSubString)
                if (inputString.charAt(j) != subString.charAt(jSubString)) break;
            if (jSubString == subStringLength) return true;
        }
        return false;
    }
}
