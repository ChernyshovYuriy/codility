package algorithms;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 09.07.14
 * Time: 22:09
 */
public class MaxSliceSwap {

    public static int solution(int[] A) {
        int result = 0;
        int length = A.length;
        if (length < 1 || length > 100000) {
            return result;
        }
        int[] batchData = new int[]{0, 0};
        int[] batchDataMax = new int[]{0, 0};
        int[][] batches = new int[length][2];
        int batchesCounter = 0;
        boolean isNewBatch = true;
        for (int i = 0; i < length; i++) {
            if (A[i] > 0) {
                batchData[1] += A[i];
                if (A[i] > batchData[0]) {
                    batchData[0] = A[i];
                }
                if (isNewBatch) {
                    batches[batchesCounter++] = batchData;
                    isNewBatch = false;
                }
            } else {
                if (batchData[1] > batchDataMax[1]) {
                    batchDataMax = batchData;
                }
                batchData = new int[2];
                isNewBatch = true;
            }
        }
        if (batchesCounter == 0) {
            return result;
        }
        if (batchData[1] > batchDataMax[1]) {
            batchDataMax = batchData;
        }
        Logger.printMessage("Max Batch:" + batchDataMax[0] + " " + batchDataMax[1]);
        int maxValueToSwap = 0;
        for (int i = 0; i < batchesCounter; i++) {
            if (batches[i][1] == batchDataMax[1]) {
                continue;
            }
            if (batches[i][0] > maxValueToSwap) {
                maxValueToSwap = batches[i][0];
            }
            Logger.printMessage("Batches:" + batches[i][0] + " " + batches[i][1]);
        }
        result = batchDataMax[1] + maxValueToSwap;
        return result;
    }

    public static void main(String[] args) {
        int[] data = new int[]{3,2,-6,3,1}; // 9
        //data = new int[]{-2, -2, 6, -1, -9, -3, 1, 6}; // 13
        //data = new int[]{-9, 4, 1, -1, -7, 4, 5, 6}; // 19
        Logger.printMessage("Solution:" + solution(data));
    }
}