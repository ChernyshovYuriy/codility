package algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/22/13
 * Time: 12:07 PM
 */
public class Addition {

    // O (N * N)
    public long solution_A(long n) {
        long result = 0;
        for (long  i = 0; i < n; i++) {
            for (long j = 0; j < i + 1; j++) {
                result++;
            }
        }
        return result;
    }

    // O (N * logN)
    public long solution_B(long n) {
        long result = 0;
        for (long i = 0; i < n; i++) {
            result += (i + 1);
        }
        return result;
    }

    // O (1)
    public long solution_C(long n) {
        return n * (n + 1) >> 1;
    }
}