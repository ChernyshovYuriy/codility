package algorithms;

import java.util.Arrays;
import java.util.Random;

public class KthMaxElement {

    public static void main(String[] args) {
        new KthMaxElement();
    }

    private final Random random;

    /**
     * Use order statistics.
     * <p>
     * Using the concept of partitioning the array (same as quick sort) which places the pivot
     * at the right position each time when called.
     * Also, it returns the index of the pivot.
     * This is the key to problem.
     * <b>Random partitioning to sort array in descending order
     * by choosing a random pivot and placing it at the right position.</b>
     * The first call to the random partition will take the entire array and return the index
     * of the pivot.
     * Now, check if this is the index we are looking for.
     * If yes - the kth largest element found, else if index >= k, then send the left sub-array
     * for partitioning (from start until index — 1).
     * Remember that index is already contains the element at its correct position.
     * If index < k, send right sub-array for partitioning (from index + 1 until end).
     * This is because sorting is in descending order.
     * <p>
     * Running time complexity of O(n).
     * Assuming that array is unsorted and there are no duplicates.
     */
    private KthMaxElement() {
        super();
        random = new Random();

        final int data[] = new int[]{3, 2, 1, 5, 6, 4};

        for (int i = 0; i < data.length; i++) {
            final int result = getKthMaxElement(data.clone(), i + 1);
            System.out.println(
                    String.format("%d max value of %s is %d", i + 1, Arrays.toString(data),result)
            );
        }
    }

    private int getKthMaxElement(final int[] data, final int k) {
        if (data.length == 1) {
            return data[0];
        }
        return randomSelect(data, 0, data.length - 1, k);
    }

    private int randomSelect(final int[] data, final int start, final int end, final int k) {
        final int index = randomPartition(data, start, end);
        if (index == k - 1) {
            return data[index];
        } else if (index >= k) {
            return randomSelect(data, start, index - 1, k);
        } else {
            return randomSelect(data, index + 1, end, k);
        }
    }

    private int randomPartition(final int[] data, final int start, final int end) {
        final int randomIndex = getRandomIndex(start, end);
        exchange(data, randomIndex, end);

        final int pivot = data[end];
        int pointer = start - 1;
        for (int i = start; i < end; ++i) {
            if (data[i] >= pivot) {
                pointer++;
                exchange(data, i, pointer);
            }
        }
        exchange(data, pointer + 1, end);
        return pointer + 1;
    }

    private int getRandomIndex(final int min, final int max) {
        final int limit = (max - min + 1);
        return random.nextInt(limit) + min;
    }

    private void exchange(final int[] data, final int index1, final int index2) {
        final int temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }
}
