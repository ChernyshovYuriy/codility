package algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 10/10/13
 * Time: 10:28 PM
 */
public class City {

    private double x;
    private double y;

    // Constructs a city at chosen x, y location
    public City(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Gets city's x coordinate
    public double getX() {
        return this.x;
    }

    // Gets city's y coordinate
    public double getY() {
        return this.y;
    }

    // Gets the distance to given city
    public double distanceTo(City city) {
        double xDistance = Math.abs(getX() - city.getX());
        double yDistance = Math.abs(getY() - city.getY());
        return Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
    }

    @Override
    public String toString() {
        return getX() + ", " + getY();
    }
}