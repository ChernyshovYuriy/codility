package algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/10/13
 * Time: 1:15 PM
 */
public class MaxFlagsNumber {

    public int execute(int[] inputData) {
        int cycleLength = inputData.length;
        int flagsNumber = 0;
        if (cycleLength > 100000 || cycleLength < 1) {
            return flagsNumber;
        }
        int peaksNumber = 0;
        for (int i = 1; i < inputData.length - 1; i++) {
            if (!(inputData[i] > inputData[i - 1] && inputData[i] > inputData[i + 1])) {
                continue;
            }
            peaksNumber++;
        }
        if (peaksNumber == 0) {
            return flagsNumber;
        }
        flagsNumber = 2;
        int theoreticalMaxFlags;
        for (int i = 0; i < cycleLength; i++) {
            theoreticalMaxFlags = getTheoreticalMaxFlags(inputData, flagsNumber);
            if (theoreticalMaxFlags >= flagsNumber) {
                if (flagsNumber == peaksNumber) {
                    break;
                }
                flagsNumber++;
            } else {
                flagsNumber -= 1;
                break;
            }
        }
        return flagsNumber;
    }

    private int getTheoreticalMaxFlags(int[] inputData, int threshold) {
        int prevIndex = -1;
        int flags = 0;
        for (int i = 1; i < inputData.length - 1; i++) {
            if (!(inputData[i] > inputData[i - 1] && inputData[i] > inputData[i + 1])) {
                continue;
            }
            if (prevIndex < 0) {
                prevIndex = i;
                flags++;
                continue;
            }
            if (i - prevIndex < threshold) {
                continue;
            }
            flags++;
            prevIndex = i;
        }
        return flags;
    }
}