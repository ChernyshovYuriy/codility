package algorithms;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 09.07.14
 * Time: 21:08
 */
public class TieRopes {

    public static int solution(int K, int[] A) {
        int result = 0;
        int length = A.length;
        if (length < 1 || length > 100000) {
            return result;
        }
        if (K < 1 || K > 1000000000) {
            return result;
        }
        int i = length;
        int sum;
        while (i > 0) {
            sum = 0;
            while (sum < K) {
                sum += A[length - i];
                i--;
                if (i == 0) {
                    break;
                }
            }
            if (sum >= K) {
                result++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] data = new int[]{1,2,3,4,1,1,3};
        data = new int[]{1};
        Logger.printMessage("Solution:" + solution(4, data));
    }
}