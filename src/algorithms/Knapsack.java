package algorithms;

import java.util.List;

/**
 * Created by ChernyshovYuriy on 9/24/13
 */
public class Knapsack {

    private int capacity;
    private List<KnapsackEntity> data;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<KnapsackEntity> getData() {
        return data;
    }

    public void setData(List<KnapsackEntity> data) {
        this.data = data;
    }
}