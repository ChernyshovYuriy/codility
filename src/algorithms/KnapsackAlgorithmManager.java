package algorithms;

import utils.Logger;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ChernyshovYuriy on 9/24/13
 */
public class KnapsackAlgorithmManager {

    public void execute(List<KnapsackEntity> data, int knapsackSize) {
        //int[][] solution = getSolutionMatrix(data, knapsackSize);
        solveKnapsackOneArray(data, knapsackSize);

        /*int[] weights = new int[data.size()];
        int i = 0;
        for (KnapsackEntity entity : data) {
            weights[i] = entity.getWeight();
            i++;
        }*/
        //int[] optimalSubset = getOptimalSubset(solution, weights);

        //printMatrix(solution, "Solution Matrix");
        //printArray(optimalSubset, "Optimal Subset");
        //Logger.printMessage("Solution: " + solution[data.size()][knapsackSize]);
    }

    /**
     * Returns the solution matrix for the KnapsackAlgorithmManager problem associated with the
     * given values, weights and knapsack capacity.
     *
     * @param capacity The capacity of the knapsack.
     */
    private int[][] getSolutionMatrix(List<KnapsackEntity> data, int capacity) {
        int dataSize = data.size();
        int[][] matrix = new int[dataSize + 1][capacity + 1];
        KnapsackEntity entity;
        for (int i = 1; i <= dataSize; i++) {
            for (int j = 0; j <= capacity; j++) {
                entity = data.get(i - 1);
                if (j - entity.getWeight() >= 0) {
                    matrix[i][j] = Math.max(matrix[i - 1][j], entity.getValue() + matrix[i - 1][j - entity.getWeight()]);
                } else {
                    matrix[i][j] = matrix[i - 1][j];
                }
            }
        }
        return matrix;
    }

    /**
     * Solves Knapsack problem with one array instead of solution matrix. Doesn't track the items
     * that are part of the optimal solution.
     * @param data
     * @param capacity
     */
    public void solveKnapsackOneArray(List<KnapsackEntity> data, int capacity) {
        int itemsCount = data.size();
        long[] solutions = new long[capacity + 1];
        KnapsackEntity currentItem;
        for (int i = 0; i <= itemsCount; i++) {
            for (int w = capacity; w > 0; w--) {
                if (i == 0) {
                    solutions[w] = 0;
                    continue;
                }
                currentItem = data.get(i - 1);
                if (currentItem.getWeight() <= w) {
                    solutions[w] = Math.max(
                            currentItem.getValue() + solutions[w - currentItem.getWeight()],
                            solutions[w]);
                }
            }
        }

        long maxValue = solutions[capacity];

        Logger.printMessage("Big data solution: " + maxValue);
    }

    /**
     * Returns the optimal subset of items that should be included in the knapsack
     * given a completed solution matrix.
     *
     * @param solutionMatrix An N by W matrix, where N is the number of items and W
     *                       is the capacity of the knapsack.
     * @param weights        An array of size N containing the weights of each of the items.
     */
    private static int[] getOptimalSubset(int[][] solutionMatrix, int[] weights) {
        int[] subset = new int[weights.length];
        int numItems = 0;
        int i = solutionMatrix.length - 1;
        for (int j = solutionMatrix[0].length - 1; j >= 0 && i > 0; i--) {
            // If the item is in the optimal subset, add it and subtract its weight
            // from the column we are checking.
            if (solutionMatrix[i][j] != solutionMatrix[i - 1][j]) {
                subset[numItems] = i;
                j -= weights[i - 1];
                numItems++;
            }
        }
        return Arrays.copyOfRange(subset, 0, numItems);
    }

    /**
     * Prints an array to the console, applying the given title.
     */
    private static void printArray(int[] array, String title) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n").append(title).append(":\n{");
        if (array.length != 0) {
            builder.append(array[0]);
        }
        for (int i = 1; i < array.length; i++) {
            builder.append(", ").append(array[i]);
        }
        builder.append("}");
        Logger.printMessage(builder.toString());

    }

    /**
     * Prints a matrix (2-dimensional array) to the console, applying the given title.
     */
    private static void printMatrix(int[][] matrix, String title) {
        StringBuilder builder = new StringBuilder();
        builder.append(title).append(":\n");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                builder.append(matrix[i][j]).append("\t");
            }
            builder.append("\n");
        }
        Logger.printMessage(builder.toString());
    }
}