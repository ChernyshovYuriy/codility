package algorithms;

/**
 * Created by ChernyshovYuriy on 9/24/13
 */
public class KnapsackEntity {

    private int value;
    private int weight;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}