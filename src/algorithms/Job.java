package algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 9/5/13
 * Time: 8:19 PM
 */
public class Job {

    private int weight;
    private int length;

    public Job() {

    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getDifferenceFactor() {
        return weight - length;
    }

    public float getRatioFactor() {
        return ((float) weight / (float) length);
    }

    @Override
    public String toString() {
        return "Weight: " + weight + " Lenght: " + length + " Difference: " + getDifferenceFactor() + " Ratio: " +
                getRatioFactor();
    }
}