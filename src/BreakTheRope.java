import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 18:32
 */
public class BreakTheRope {

    public static void main(String[] args) {
        final BreakTheRope breakTheRope = new BreakTheRope();

        // 3
        int[] A = new int[]{5,3,6,3,3};
        int[] B = new int[]{2,3,1,1,2};
        int[] C = new int[]{-1,0,-1,0,3};

        // 2
        A = new int[]{4,3,1};
        B = new int[]{2,2,1};
        C = new int[]{-1,0,1};

        // 3
        A = new int[]{9, 6, 6, 10, 7, 7, 7, 7, 6, 8};
        B = new int[]{2, 4, 3, 3, 2, 5, 2, 3, 3, 5};
        C = new int[]{-1, 0, -1, 1, 0, 2, 0, -1, 4, 8};

        int l = 100000;
        A = new int[l];
        B = new int[l];
        C = new int[l];
        C[0] = -1;
        C[1] = -1;
        for (int i = 0; i < l; i++) {
            A[i] = l - i;
            B[i] = 3;
            if (i > 1) {
                C[i] = i - 2;
            }
        }

        //([8, 7, 8, 10, 8, 7, 6, 6, 7, 10], [2, 1, 5, 4, 1, 3, 3, 1, 1, 3], [-1, 0, 0, -1, 3, 4, 3, 6, 2, -1])
        //with WRONG ANSWER [ got 5 expected 6 ]

        long startTime = System.currentTimeMillis();
        System.out.println("Solution:" + breakTheRope.solution(A, B, C) + " in " +
                (System.currentTimeMillis() - startTime) + " ms");
    }

    public int solution(int[] A, int[] B, int[] C) {
        int result = 0;
        int N = A.length;
        if (N > 100000 || N == 0) {
            return result;
        }
        Node node;
        Node rootNode;
        Node prevNode;
        int value;
        int durability;
        int nodePosition;
        final Node[] nodes = new Node[N];
        for (int i = 0; i < N; i++) {
            value = B[i];
            durability = A[i];
            nodePosition = C[i];
            node = new Node(value, durability);
            nodes[i] = node;
            if (nodePosition == -1) {
                if (value > node.durability) {
                    return i;
                }
                continue;
            }
            node.prevNode = nodePosition;
            rootNode = nodes[nodePosition];
            prevNode = rootNode;
            for (int a : A) {
                if ((prevNode.sum += value) > prevNode.durability) {
                    return i;
                }
                int prevNodeId = prevNode.prevNode;
                if (prevNodeId == -1) {
                    break;
                }
                prevNode = nodes[prevNodeId];
            }
        }
        return N;
    }

    private class Node {

        int prevNode = -1;
        int sum;
        final int value;
        final int durability;

        private Node(int value, int durability) {
            this.value = value;
            this.durability = durability;
            sum = value;
        }
    }
}