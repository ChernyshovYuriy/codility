package collections;

/**
 * Created by ChernyshovYuriy on 9/23/13
 */
public class LinkedListEntity<T> {

    private T entityValue;
    private LinkedListEntity<T> nextEntityPointer;

    public LinkedListEntity(T entityValue) {
        this.entityValue = entityValue;
    }

    public T getEntityValue() {
        return entityValue;
    }

    public void setEntityValue(T entityValue) {
        this.entityValue = entityValue;
    }

    public LinkedListEntity<T> getNextEntityPointer() {
        return nextEntityPointer;
    }

    public void setNextEntityPointer(LinkedListEntity<T> nextEntityPointer) {
        this.nextEntityPointer = nextEntityPointer;
    }
}