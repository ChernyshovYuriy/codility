package collections;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 17:08
 */
public class BoundedConcurrentStack implements SimpleStack {

    private final Object[] stack;
    private int top;
    private final int max;

    public BoundedConcurrentStack() {
        this(50);
    }

    public BoundedConcurrentStack(int max_size) {
        top = 0;
        max = max_size;
        stack = new Object[max];
    }

    @Override
    public synchronized void push(Object item) {
        while (isFull()) {
            try {
                wait();
            } catch (InterruptedException ex) {

            }
        }
        stack[top] = item;
        top++;
        notifyAll();
    }

    @Override
    public synchronized Object pop() {
        while (isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) {

            }
        }
        top--;
        final Object returnObject = stack[top];
        notifyAll();
        return returnObject;
    }

    @Override
    public synchronized Object top() {
        while (this.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) {

            }
        }
        return stack[top - 1];
    }

    protected boolean isEmpty() {
        return top == 0;
    }

    protected boolean isFull() {
        return top == max;
    }
}