package collections;

import java.util.Comparator;

public class HeapMaxComparator implements Comparator {

    public int compare(Object left, Object right) throws ClassCastException {
        int result = 0;
        int leftInt = Integer.valueOf((Integer) left);
        int rightInt = Integer.valueOf((Integer) right);
        if (leftInt > rightInt) {
            result = 1;
        } else if (rightInt > leftInt) {
            result = -1;
        }
        //System.out.println("Comparator " + leftInt + " " + rightInt + " " + result);
        return result;
    }
}
