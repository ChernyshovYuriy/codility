package collections;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * A Heap that uses a heap-ordered ArrayList internally.
 *
 */
public class Heap<T> {

    /** The underlying list. */
    private ArrayList<T> arrayList;

    /** The comparator to determine priority. */
    private Comparator comparator;

    /**
     * Constructor. Uses a ArrayList as the underlying list.
     *
     * @param comparator The comparator to determine priority.
     */
    public Heap(Comparator comparator) {
        if (comparator == null) {
            System.out.println("Comparator can not be null");
            return;
        }
        this.comparator = comparator;
        arrayList = new ArrayList<T>();
    }

    public void enqueue(T value) {
        arrayList.add(value);
        bubbleUp(arrayList.size() - 1);
    }

    public T dequeue() throws EmptyQueueException {
        if (isEmpty()) {
            throw new EmptyQueueException();
        }
        T result = arrayList.get(0);
        if (arrayList.size() > 1) {
            arrayList.set(0, arrayList.get(arrayList.size() - 1));
            bubbleDown(0);
        }
        arrayList.remove(arrayList.size() - 1);
        return result;
    }

    public T getHead() {
        return arrayList.get(0);
    }

    public void clear() {
        arrayList.clear();
    }

    public int size() {
        return arrayList.size();
    }

    public boolean isEmpty() {
        return arrayList.isEmpty();
    }

    @Override
    public String toString() {
        return "";
    }

    private void bubbleUp(int index) {
        if (index == 0) {
            return;
        }
        int parent = (index - 1) / 2;
        if (comparator.compare(arrayList.get(index), arrayList.get(parent)) > 0) {
            swap(index, parent);
            bubbleUp(parent);
        }
    }

    private void bubbleDown(int index) {
        int left = index * 2 + 1;
        int right = index * 2 + 2;

        if (left >= arrayList.size()) {
            return;
        }

        int largestChild = left;
        if (right < arrayList.size()) {
            if (comparator.compare(arrayList.get(left), arrayList.get(right)) < 0) {
                largestChild = right;
            }
        }

        if (comparator.compare(arrayList.get(index), arrayList.get(largestChild)) < 0) {
            swap(index, largestChild);
            bubbleDown(largestChild);
        }
    }

    private void swap(int index1, int index2) {
        T temp = arrayList.get(index1);
        arrayList.set(index1, arrayList.get(index2));
        arrayList.set(index2, temp);
    }
}