package collections;

/**
 * Created by ChernyshovYuriy on 9/23/13
 */
public class LinkedListAnalyzer {

    public LinkedListEntity findKthToTail(LinkedListEntity pListHead, int k) {
        if (pListHead == null || k <= 0) {
            return null;
        }
        LinkedListEntity pAhead = pListHead;
        for (int i = 0; i < k - 1; i++) {
            if (pAhead.getNextEntityPointer() != null) {
                pAhead = pAhead.getNextEntityPointer();
            } else {
                return null;
            }
        }
        LinkedListEntity pBehind = pListHead;
        while (pAhead.getNextEntityPointer() != null) {
            pAhead = pAhead.getNextEntityPointer();
            pBehind = pBehind.getNextEntityPointer();
        }
        return pBehind;
    }
    
    public boolean hasLoop(LinkedListEntity<Integer> pHead) {
        if (pHead == null) {
            return false;
        }

        LinkedListEntity pSlow = pHead.getNextEntityPointer();
        if (pSlow == null) {
            return false;
        }

        LinkedListEntity pFast = pSlow.getNextEntityPointer();
        while (pFast != null && pSlow != null) {
            if (pFast == pSlow) {
                return true;
            }
            pSlow = pSlow.getNextEntityPointer();
            pFast = pFast.getNextEntityPointer();
            if (pFast != null) {
                pFast = pFast.getNextEntityPointer();
            }
        }

        return false;
    }

    public LinkedListEntity meetingNode(LinkedListEntity pHead) {
        if (pHead == null) {
            return null;
        }

        LinkedListEntity pSlow = pHead.getNextEntityPointer();
        if (pSlow == null) {
            return null;
        }

        LinkedListEntity pFast = pSlow.getNextEntityPointer();
        while (pFast != null && pSlow != null) {
            if (pFast == pSlow) {
                return pFast;
            }
            pSlow = pSlow.getNextEntityPointer();
            pFast = pFast.getNextEntityPointer();
            if (pFast != null) {
                pFast = pFast.getNextEntityPointer();
            }
        }

        return null;
    }

    public LinkedListEntity entryNodeOfLoop(LinkedListEntity pHead) {
        LinkedListEntity meetingNode = meetingNode(pHead);
        if (meetingNode == null) {
            return null;
        }

        // get the number of nodes in loop
        int nodesInLoop = 1;
        LinkedListEntity pNode1 = meetingNode;
        while (pNode1.getNextEntityPointer() != meetingNode) {
            pNode1 = pNode1.getNextEntityPointer();
            nodesInLoop++;
        }

        // move pNode1
        pNode1 = pHead;
        for (int i = 0; i < nodesInLoop; i++) {
            pNode1 = pNode1.getNextEntityPointer();
        }

        // move pNode1 and pNode2
        LinkedListEntity pNode2 = pHead;
        while (pNode1 != pNode2) {
            pNode1 = pNode1.getNextEntityPointer();
            pNode2 = pNode2.getNextEntityPointer();
        }

        return pNode1;
    }
}