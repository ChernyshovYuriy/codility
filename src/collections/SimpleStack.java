package collections;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 17:08
 */
public interface SimpleStack {

    public void push(Object item);

    public Object pop();

    public Object top();
}