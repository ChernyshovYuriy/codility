package OOPExercises.vendingMachine.storage;

import OOPExercises.vendingMachine.enums.Item;

public class StorageImpl implements Storage {

    // TODO: For simplicity, keep all Items in the same array
    private final Item[] data;

    private final int maxStorage;
    private int index;

    public StorageImpl(final int maxStorage) {
        super();
        index = 0;
        this.maxStorage = maxStorage;
        data = new Item[maxStorage];
    }

    @Override
    public boolean add(final Item item) {
        if (item == Item.UNKNOWN) {
            return false;
        }
        if (index >= maxStorage) {
            return false;
        }
        data[index++] = item;
        return true;
    }

    @Override
    public Item select(final Item item) {
        Item returnItem = Item.UNKNOWN;
        if (item == Item.UNKNOWN) {
            return returnItem;
        }
        for (int i = 0; i < maxStorage; ++i) {
            if (data[i] == item) {
                returnItem = data[i];
                data[i] = null;
                --index;
                break;
            }
        }
        return returnItem;
    }
}
