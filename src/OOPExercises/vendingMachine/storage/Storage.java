package OOPExercises.vendingMachine.storage;

import OOPExercises.vendingMachine.enums.Item;

public interface Storage {

    boolean add(Item item);

    Item select(Item item);
}
