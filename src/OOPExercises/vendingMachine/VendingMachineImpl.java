package OOPExercises.vendingMachine;

import OOPExercises.vendingMachine.enums.Command;
import OOPExercises.vendingMachine.enums.Item;
import OOPExercises.vendingMachine.enums.PaymentMethod;
import OOPExercises.vendingMachine.payment.cmd.CashPaymentCmd;
import OOPExercises.vendingMachine.payment.cmd.CreditCardPaymentCmd;
import OOPExercises.vendingMachine.payment.Payment;
import OOPExercises.vendingMachine.storage.Storage;
import OOPExercises.vendingMachine.storage.StorageImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class VendingMachineImpl implements VendingMachine {

    public static void main(final String[] args) {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        final VendingMachineImpl machine = new VendingMachineImpl();

        boolean doLoop = true;
        String input = null;
        while (doLoop) {
            System.out.print("Print command/value:");
            try {
                input = reader.readLine();
            } catch (final IOException e) {
                System.err.println("Invalid input:" + e);
            }

            // TODO: Refactor to use parser and command pattern

            if (input == null || input.isEmpty()) {
                continue;
            }
            final String[] values = input.split(" ");
            final String commandName = values[0];
            final String commandValue = values.length > 1 ? values[1] : "";

            Command command = Command.getCommand(commandName);
            switch (command) {
                case EXIT:
                    doLoop = false;
                    break;
                case ADD:
                    machine.addItem(Item.getItem(commandValue));
                    break;
                case SELECT:
                    machine.select(Item.getItem(commandValue));
                    break;
                case PAY:
                    machine.pay(PaymentMethod.getPaymentMethod(commandValue));
                    break;
                case DISPENSE:
                    machine.dispense();
                    break;
                default:
                    System.err.println("Unknown command");
                    break;
            }
        }
    }

    private final Storage storage;
    private final Payment payment;
    private Item selectedItem;
    private boolean paymentResult;

    public VendingMachineImpl() {
        super();
        System.out.println("Vendor Machine started");

        selectedItem = null;
        paymentResult = false;

        final int maxStorage = 10;
        storage = new StorageImpl(maxStorage);

        payment = new Payment();
        payment.register(PaymentMethod.CARD, new CreditCardPaymentCmd());
        payment.register(PaymentMethod.CASH, new CashPaymentCmd());
    }

    @Override
    public final void addItem(final Item item) {
        final boolean result = storage.add(item);
        System.out.println(
                result
                        ? "Item " + item + " inserted."
                        : "Storage is full. Item " + item + " not inserted."
        );
    }

    @Override
    public final void select(final Item item) {
        selectedItem = storage.select(item);
        System.out.println(
                selectedItem != Item.UNKNOWN
                        ? selectedItem + " selected"
                        : "Can not select " + item
        );
    }

    @Override
    public final void pay(final PaymentMethod paymentMethod) {
        if (selectedItem == Item.UNKNOWN) {
            System.out.println("Can not do paymentMethod. Item is not selected");
            return;
        }
        paymentResult = payment.execute(selectedItem, paymentMethod);
        System.out.println(
                paymentResult
                        ? "Payment for " + selectedItem + " with " + paymentMethod + " succeeded"
                        : "Can not pay for " + selectedItem + " with " + paymentMethod
        );
    }

    @Override
    public void dispense() {
        if (selectedItem == Item.UNKNOWN) {
            System.out.println("Can not dispense item. Please select it first.");
            return;
        }
        if (!paymentResult) {
            System.out.println("Can not dispense item. Please pay for it first.");
            return;
        }
        System.out.println("Dispense " + selectedItem);

        reset();
    }

    private void reset() {
        selectedItem = Item.UNKNOWN;
        paymentResult = false;
    }
}
