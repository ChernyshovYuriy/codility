package OOPExercises.vendingMachine;

import OOPExercises.vendingMachine.enums.Item;
import OOPExercises.vendingMachine.enums.PaymentMethod;

public interface VendingMachine {

    void addItem(final Item item);

    void select(final Item item);

    void pay(final PaymentMethod paymentMethod);

    void dispense();
}
