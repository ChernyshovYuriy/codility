package OOPExercises.vendingMachine.payment;

import OOPExercises.vendingMachine.enums.Item;
import OOPExercises.vendingMachine.enums.PaymentMethod;
import OOPExercises.vendingMachine.payment.cmd.PaymentCmd;

import java.util.HashMap;
import java.util.Map;

public class Payment {

    private final Map<PaymentMethod, PaymentCmd> methods;

    public Payment() {
        super();
        methods = new HashMap<>();
    }

    public void register(final PaymentMethod method, final PaymentCmd cmd) {
        methods.put(method, cmd);
    }

    public boolean execute(final Item item, final PaymentMethod method) {
        final PaymentCmd cmd = methods.get(method);
        if (cmd == null) {
            System.err.println("No command registered for " + method);
            return false;
        }
        return cmd.pay(item);
    }
}
