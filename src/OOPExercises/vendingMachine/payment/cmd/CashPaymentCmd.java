package OOPExercises.vendingMachine.payment.cmd;

import OOPExercises.vendingMachine.enums.Item;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CashPaymentCmd implements PaymentCmd {

    public CashPaymentCmd() {
        super();
    }

    @Override
    public boolean pay(final Item item) {
        // TODO: Farther improvement here
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        boolean doLoop = true;
        while (doLoop) {
            System.out.print("Insert " + item.getPrice() + " CAD: ");
            try {
                input = reader.readLine();
            } catch (final IOException e) {
                System.err.println("Invalid input:" + e);
            }

            if (input == null || input.isEmpty()) {
                continue;
            }
            if (isNumeric(input)) {
                doLoop = false;
            }
        }
        return validatePayment(item.getPrice(), Integer.valueOf(input));
    }

    private boolean isNumeric(final String str) {
        // Match a number with optional '-' and decimal.
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    private boolean validatePayment(final int price, final int payedAmount) {
        return payedAmount >= price;
    }
}
