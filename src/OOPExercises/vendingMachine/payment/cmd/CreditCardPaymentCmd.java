package OOPExercises.vendingMachine.payment.cmd;

import OOPExercises.vendingMachine.enums.Item;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CreditCardPaymentCmd implements PaymentCmd {

    public CreditCardPaymentCmd() {
        super();
    }

    @Override
    public boolean pay(final Item item) {
        // TODO: Farther improvement here
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        boolean doLoop = true;
        while (doLoop) {
            System.out.print("Accept transaction of " + item.getPrice() + " CAD? Y/N ");
            try {
                input = reader.readLine();
            } catch (final IOException e) {
                System.err.println("Invalid input:" + e);
            }

            if ("Y".equalsIgnoreCase(input) || "N".equalsIgnoreCase(input)) {
                doLoop = false;
            }
        }
        if ("Y".equalsIgnoreCase(input)) {
            return true;
        }
        if ("N".equalsIgnoreCase(input)) {
            return false;
        }
        return false;
    }
}
