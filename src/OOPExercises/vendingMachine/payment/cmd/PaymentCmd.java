package OOPExercises.vendingMachine.payment.cmd;

import OOPExercises.vendingMachine.enums.Item;

public interface PaymentCmd {

    boolean pay(final Item item);
}
