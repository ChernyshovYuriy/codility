package OOPExercises.vendingMachine.enums;

public enum Command {
    UNKNOWN("unknown"),
    ADD("add"),
    SELECT("select"),
    PAY("pay"),
    DISPENSE("dispense"),
    EXIT("exit");

    private final String command;

    Command(final String command) {
        this.command = command;
    }

    public static Command getCommand(final String value) {

        // TODO: This is quick solution and to be refactored.

        if (value == null || value.isEmpty()) {
            return UNKNOWN;
        }
        if (ADD.command.equalsIgnoreCase(value)) {
            return ADD;
        }
        if (SELECT.command.equalsIgnoreCase(value)) {
            return SELECT;
        }
        if (PAY.command.equalsIgnoreCase(value)) {
            return PAY;
        }
        if (DISPENSE.command.equalsIgnoreCase(value)) {
            return DISPENSE;
        }
        if (EXIT.command.equalsIgnoreCase(value)) {
            return EXIT;
        }
        return UNKNOWN;
    }
}
