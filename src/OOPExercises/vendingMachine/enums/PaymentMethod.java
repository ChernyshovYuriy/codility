package OOPExercises.vendingMachine.enums;

public enum PaymentMethod {
    UNKNOWN("unknown"),
    CARD("card"),
    CASH("cash");

    private final String name;

    PaymentMethod(final String name) {
        this.name = name;
    }

    public static PaymentMethod getPaymentMethod(final String value) {

        // TODO: This is quick solution and to be refactored.

        if (value == null || value.isEmpty()) {
            return UNKNOWN;
        }
        if (CARD.name.equalsIgnoreCase(value)) {
            return CARD;
        }
        if (CASH.name.equalsIgnoreCase(value)) {
            return CASH;
        }
        return UNKNOWN;
    }
}
