package OOPExercises.vendingMachine.enums;

public enum Item {
    UNKNOWN("unknown", 0),
    WATER("water", 1),
    JUICE("juice", 2),
    SNACK_BAR("snack_bar", 5),
    SANDWICH("sandwich", 10);

    private final String name;
    private final int price;

    Item(final String name, final int price) {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "{" + name + " : " + price + "}";
    }

    public static Item getItem(final String value) {

        // TODO: This is quick solution and to be refactored.

        if (value == null || value.isEmpty()) {
            return UNKNOWN;
        }
        if (WATER.name.equalsIgnoreCase(value)) {
            return WATER;
        }
        if (JUICE.name.equalsIgnoreCase(value)) {
            return JUICE;
        }
        if (SNACK_BAR.name.equalsIgnoreCase(value)) {
            return SNACK_BAR;
        }
        if (SANDWICH.name.equalsIgnoreCase(value)) {
            return SANDWICH;
        }
        return UNKNOWN;
    }
}
