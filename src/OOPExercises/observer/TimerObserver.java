package OOPExercises.observer;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 16:18
 */
public class TimerObserver implements EventObserver {

    @Override
    public void update(EventState eventState) {
        Logger.printMessage("Timer Observer receive state changed to:" + eventState);
    }
}