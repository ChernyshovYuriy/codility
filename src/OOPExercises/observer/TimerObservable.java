package OOPExercises.observer;

import utils.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 15:58
 */
public class TimerObservable implements EventObservable {

    private static final Object MUTEX = new Object();

    private List<EventObserver> observers = new ArrayList<EventObserver>();

    public EventState eventState = EventState.DEFAULT;

    private boolean observersHasChanged = false;

    public TimerObservable() {

    }

    public EventState getEventState() {
        return eventState;
    }

    public void setEventState(EventState eventState) {
        this.eventState = eventState;
    }

    @Override
    public void register(EventObserver observer) {
        if (observer == null) {
            throw new NullPointerException("Can not register observer with a null reference");
        }
        synchronized (MUTEX) {
            if (observers.contains(observer)) {
                return;
            }
            Logger.printMessage("Timer Observable register new observer:" + observer);
            observers.add(observer);
        }
    }

    @Override
    public void unregister(EventObserver observer) {
        if (observer == null) {
            throw new NullPointerException("Can not unregister observer with a null reference");
        }
        synchronized (MUTEX) {
            Logger.printMessage("Timer Observable unregister observer:" + observer);
            observers.remove(observer);
        }
    }

    @Override
    public void notifyStateChange() {
        List<EventObserver> observersLocal;
        // synchronization is used to make sure any observer registered after message is received is not notified
        synchronized (MUTEX) {
            if (!observersHasChanged) {
                return;
            }
            observersLocal = new ArrayList<EventObserver>(observers);
            observersHasChanged = false;
        }

        for (EventObserver eventObserver : observersLocal) {
            Logger.printMessage("Timer Observable notify state changed to:" + eventState);
            eventObserver.update(eventState);
        }
    }

    public void updateEventState() {
        observersHasChanged = true;
        notifyStateChange();
    }
}