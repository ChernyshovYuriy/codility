package OOPExercises.observer;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 15:52
 */
public enum  EventState {

    DEFAULT,
    STATE_A,
    STATE_B
}