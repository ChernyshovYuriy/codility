package OOPExercises.observer;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 16:22
 */
public class ObserverMain {

    public static void main(String[] args) {
        final TimerObservable timerObservable = new TimerObservable();
        final TimerObserver timerObserver_A = new TimerObserver();
        final TimerObserver timerObserver_B = new TimerObserver();
        final TimerObserver timerObserver_C = new TimerObserver();

        timerObservable.register(timerObserver_A);
        timerObservable.register(timerObserver_B);
        timerObservable.register(timerObserver_C);

        final boolean[] trigger = {false};
        final int[] counter = {10};

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                EventState eventState;
                if (trigger[0]) {
                    eventState = EventState.STATE_A;
                } else {
                    eventState = EventState.STATE_B;
                }

                timerObservable.setEventState(eventState);
                timerObservable.updateEventState();

                trigger[0] = !trigger[0];

                if (counter[0]-- == 0) {
                    timer.cancel();
                    timer.purge();

                    timerObservable.unregister(timerObserver_A);
                    timerObservable.unregister(timerObserver_B);
                    timerObservable.unregister(timerObserver_C);
                }
            }
        }, 1000, 1000);
    }
}