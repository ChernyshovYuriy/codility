package OOPExercises.observer;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 15:43
 */
public interface EventObservable {

    /**
     * Register observer of the event
     *
     * @param observer reference to {@link OOPExercises.observer.EventObserver} instance
     */
    public void register(EventObserver observer);

    /**
     * Unregister observer of the event
     *
     * @param observer reference to {@link OOPExercises.observer.EventObserver} instance
     */
    public void unregister(EventObserver observer);

    /**
     * Method to notify observers that subject (or observable) has been change it's state
     */
    public void notifyStateChange();
}