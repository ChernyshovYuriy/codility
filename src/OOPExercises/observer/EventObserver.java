package OOPExercises.observer;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 15:51
 */
public interface EventObserver {

    /**
     * Method to update the {@link OOPExercises.observer.EventObserver},
     * used by {@link OOPExercises.observer.EventObservable}
     *
     * @param eventState changed state of the event
     */
    public void update(EventState eventState);
}