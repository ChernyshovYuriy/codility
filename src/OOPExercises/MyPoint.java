package OOPExercises;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/4/13
 * Time: 2:15 PM
 */
public class MyPoint {

    private int x = 0;
    private int y = 0;

    public MyPoint() {

    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(int x, int y) {
        double xDifferences = Math.abs(this.x - x);
        double yDifferences = Math.abs(this.y - y);
        return Math.sqrt(Math.pow(xDifferences, 2) + Math.pow(yDifferences, 2));
    }

    public double distance(MyPoint myPoint) {
        return distance(myPoint.getX(), myPoint.getY());
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}