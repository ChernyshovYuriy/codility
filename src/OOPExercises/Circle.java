package OOPExercises;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/4/13
 * Time: 1:41 PM
 */
public class Circle {

    // private instance variable, not accessible from outside this class
    private double radius;
    private String color;

    // 1st constructor, which sets both radius and color to default
    public Circle() {
        radius = 1.0;
        color = "red";
    }

    // 2nd constructor with given radius, but color default
    public Circle(double radius) {
        this.radius = radius;
        color = "red";
    }

    // 3rd constructor with given radius, but color default
    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    // A public method for retrieving the radius
    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }

    // A public method for computing the area of circle
    public double getArea() {
        return radius * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "Circle has Radius: " + radius + ", Color: " + color + ", Area: " + getArea();
    }
}