package OOPExercises.factorymethod;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 05.07.14
 * Time: 18:04
 */
public class ProductBCreator extends ProductCreator {

    @Override
    public Product createProduct() {
        return new ProductB();
    }
}