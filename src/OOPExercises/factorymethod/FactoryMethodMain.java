package OOPExercises.factorymethod;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 05.07.14
 * Time: 18:04
 */
public class FactoryMethodMain {

    public static void main(String[] args) {

        ProductCreator[] productCreators = {new ProductACreator(), new ProductBCreator()};

        for (ProductCreator creator : productCreators) {
            Product product = creator.createProduct();
            System.out.printf("Created {%s}\n", product.getClass());
        }
    }
}