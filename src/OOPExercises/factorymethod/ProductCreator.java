package OOPExercises.factorymethod;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 05.07.14
 * Time: 18:01
 */
public abstract class ProductCreator {

    protected ProductCreator() {
        Logger.printMessage("Product Creator constructor");
    }

    public abstract Product createProduct();
}