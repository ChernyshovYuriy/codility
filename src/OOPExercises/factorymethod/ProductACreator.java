package OOPExercises.factorymethod;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 05.07.14
 * Time: 18:03
 */
public class ProductACreator extends ProductCreator {

    @Override
    public Product createProduct() {
        return new ProductA();
    }
}