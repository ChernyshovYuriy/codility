package OOPExercises.factorymethod;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 05.07.14
 * Time: 18:01
 */
public class ProductB extends Product {

    public ProductB() {
        Logger.printMessage("Product B constructor");
    }
}