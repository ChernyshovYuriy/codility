package OOPExercises.factorymethod;

import utils.Logger;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 05.07.14
 * Time: 18:00
 */
public abstract class Product {

    protected Product() {
        Logger.printMessage("Product constructor");
    }
}