package OOPExercises;

/**
 * Created with Intellij IDEA
 * User: Yuriy Chernyshov
 * Date: 5/2/14
 * Time: 12:11 PM
 */
public class Singleton {

    private Singleton() {

    }

    public static Singleton getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final Singleton INSTANCE = new Singleton();
    }
}