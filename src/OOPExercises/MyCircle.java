package OOPExercises;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/4/13
 * Time: 2:30 PM
 */
public class MyCircle {

    private MyPoint center;
    private int radius = 1;

    public MyCircle(MyPoint center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    public MyCircle(int x, int y, int radius) {
        center = new MyPoint(x, y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public MyPoint getCenter() {
        return center;
    }

    public void setCenter(MyPoint center) {
        this.center = center;
    }

    public void setCenter(int x, int y) {
        center = new MyPoint(x, y);
    }

    public double getArea() {
        return Math.PI + radius * radius;
    }

    @Override
    public String toString() {
        return "Circle @ " + center + " radius = " + radius;
    }
}