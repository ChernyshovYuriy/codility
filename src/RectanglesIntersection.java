import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 11:19 PM
 */
public class RectanglesIntersection {

    public static int rectanglesIntersection(int blx1, int bly1, int trx1, int try1, int blx2, int bly2, int trx2, int try2) {
        Rectangle rect1 = new Rectangle(blx1, bly1, trx1 - blx1, try1 - bly1);
        Rectangle rect2 = new Rectangle(blx2, bly2, trx2 - blx2, try2 - bly2);
        Rectangle intersection = rect1.intersection(rect2);
        System.out.println("Rectangle intersection " + intersection);
        int result = -1;
        if (intersection.width == 1 || intersection.height == 1) {
            result = 0;
        } else if (intersection.width < 0 && intersection.height < 0) {
            result = 0;
        } else {
            int square = intersection.height * intersection.width;
            if (square <= Integer.MAX_VALUE) {
                result = square;
            }

        }
        System.out.println("Rectangles Intersection is " + result);
        return result;
    }
}