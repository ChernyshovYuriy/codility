import java.math.BigDecimal;
import java.math.MathContext;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 11:25 PM
 */
public class DecimalRepresentation {

    public static String decimalRepresentation(int A, int B) {
        if (A < 0 && A > 1000000 && B < 0 && B > 1000000) {
            return "";
        }
        double double_A = Double.parseDouble(String.valueOf(A));
        double double_B = Double.parseDouble(String.valueOf(B));
        double divisionResult = double_A / double_B;
        MathContext precision = new MathContext(10);
        String result = new BigDecimal(divisionResult, precision).toString();

        String[] values = result.split("\\.");
        int beforePointValue = Integer.valueOf(values[0]);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(beforePointValue);

        long afterPointValue;
        if (values.length == 2) {
            afterPointValue = Long.valueOf(values[1]);
            if (afterPointValue > 0) {
                stringBuffer.append(".");
                String afterPointValueStrings = String.valueOf(afterPointValue);
                LinkedList<Character> beforeZero = new LinkedList<Character>();
                LinkedList<Character> afterZero = new LinkedList<Character>();
                boolean isZero = false;
                boolean isContainsZero = afterPointValueStrings.contains("0");
                boolean isFirstAfterZero = true;
                for (int i = 0; i < afterPointValueStrings.length(); i++) {
                    if (isContainsZero && !isZero) {
                        beforeZero.add(afterPointValueStrings.charAt(i));
                    }
                    if (!isZero && afterPointValueStrings.charAt(i) == "0".charAt(0)) {
                        isZero = true;
                    }
                    if (isContainsZero) {
                        if (isZero) {
                            if (!afterZero.contains(afterPointValueStrings.charAt(i))) {
                                if (!isFirstAfterZero) {
                                    afterZero.add(afterPointValueStrings.charAt(i));
                                }
                                isFirstAfterZero = false;
                            } else {
                                continue;
                            }
                        }
                    } else {
                        if (!afterZero.contains(afterPointValueStrings.charAt(i))) {
                            afterZero.add(afterPointValueStrings.charAt(i));
                        }
                    }
                }
                for (Object s : beforeZero.toArray()) {
                    stringBuffer.append(s.toString());
                }
                if (afterZero.size() > 0) {
                    stringBuffer.append("(");
                }
                for (Object s : afterZero.toArray()) {
                    stringBuffer.append(s.toString());
                }
                if (afterZero.size() > 0) {
                    stringBuffer.append(")");
                }
            }
        }


        System.out.println("Decimal representation " + stringBuffer.toString());
        return result;
    }
}