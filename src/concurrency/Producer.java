package concurrency;

import collections.BoundedConcurrentStack;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 17:18
 */
public class Producer implements Runnable {

    private final BoundedConcurrentStack stack;

    public Producer(BoundedConcurrentStack stack) {
        this.stack = stack;
    }

    @Override
    public void run() {
        for (int count = 1; true; count++) {
            // Will block when stack is full.
            stack.push (new Integer(count));
            System.out.println("(" + Thread.currentThread().getName() + ") pushed " + count);
        }
    }
}