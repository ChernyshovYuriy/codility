package concurrency;

import collections.BoundedConcurrentStack;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 17:22
 */
public class ProducerConsumer {

    public static void main (String args[]) {
        final int size = 10;
        final BoundedConcurrentStack stack = new BoundedConcurrentStack(size);
        System.out.println ("starting up stack with size " + size);
        final JoinableThreadGroup thread_group = new JoinableThreadGroup("Producer/Consumer");
        new Thread(thread_group, new Producer(stack), "Producer").start();
        new Thread(thread_group, new Consumer(stack), "Consumer").start();
        try {
            thread_group.join();
        } catch(InterruptedException ex) {
            System.out.println ("ThreadTest::main:" + ex.getMessage());
        }
    }
}