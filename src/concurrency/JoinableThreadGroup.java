package concurrency;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 17:26
 */
public class JoinableThreadGroup extends ThreadGroup {

    public JoinableThreadGroup (String name) {
        super (name);
    }

    public JoinableThreadGroup (ThreadGroup parent, String name) {
        super (parent, name);
    }

    // Wait for all the threads in the group to exit.
    public void join() throws InterruptedException {
        final Thread list[] = new Thread[activeCount()];
        enumerate (list, true);
        for (int i = 0; i < list.length; i++) {
            list[i].join();
        }
    }
}