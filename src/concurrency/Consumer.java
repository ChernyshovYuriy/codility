package concurrency;

import collections.BoundedConcurrentStack;

/**
 * Created with Intellij IDEA
 * Author: Yuriy Chernyshov
 * Date: 28.09.14
 * Time: 17:21
 */
public class Consumer implements Runnable {

    private final BoundedConcurrentStack stack;

    public Consumer(BoundedConcurrentStack stack) {
        this.stack = stack;
    }

    @Override
    public void run() {
        // Will block when stack is empty.
        for (;;) {
            System.out.println("(" + Thread.currentThread().getName() + ") popping " + stack.pop());
        }
    }
}