import sorting.MergeSort;
import utils.Utilities;

import java.util.ArrayList;

/**
 * Created by ChernyshovYuriy on 18.07.13.
 */
public class Inversions {

    public Inversions() {
        //2407905288
        ArrayList<Integer> integers = Utilities.loadIntegerArrayList("assets/IntegerArray.txt");

        //processInversions_BrutalWay(integers);
        int[] data = new int[integers.size()];
        for (int i = 0; i < integers.size(); i++) {
            data[i] = integers.get(i);
        }
        MergeSort.sort(data);

        /*for (int i : result) {
            System.out.println(" : " + i);
        }*/
    }

    private long processInversions_BrutalWay(ArrayList<Integer> integers) {
        long counter = 0;
        int loopSize = integers.size();
        for (int i = 0; i < loopSize; i++) {
            for (int j = i + 1; j < loopSize; j++) {
                if (integers.get(i) > integers.get(j)) {
                    counter++;
                }
            }
        }
        System.out.println("Inversions brutal     way: " + counter);
        return counter;
    }
}