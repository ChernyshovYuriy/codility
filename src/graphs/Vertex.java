package graphs;

/**
 * Created by Chernyshov Yuriy on 8/8/13
 */
public class Vertex {

    final private String id;
    final private String name;

    public Vertex(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public int getIntegerId() {
        return Integer.parseInt(id);
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (object == null)
            return false;
        if (getClass() != object.getClass())
            return false;
        Vertex vertex = (Vertex) object;
        if (id == null) {
            if (vertex.id != null)
                return false;
        } else if (!id.equals(vertex.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
}