package graphs;

import utils.Logger;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/4/13
 * Time: 10:17 AM
 */
public class AdjMatrixEdgeWeightedDigraph {

    private int verticesNumber;
    private int edgesNumber;
    private DirectedEdge[][] adjacentMatrix;

    // empty graph with verticesNumber vertices
    public AdjMatrixEdgeWeightedDigraph(int verticesNumber) {
        if (verticesNumber < 0) {
            throw new RuntimeException("Number of vertices must be non negative");
        }
        this.verticesNumber = verticesNumber;
        this.edgesNumber = 0;
        this.adjacentMatrix = new DirectedEdge[verticesNumber][verticesNumber];
    }

    // random graph with verticesNumber vertices and edgesNumber edges
    public AdjMatrixEdgeWeightedDigraph(int verticesNumber, int edgesNumber) {
        //this(verticesNumber);
        if (edgesNumber < 0) {
            throw new RuntimeException("Number of edges must be non negative");
        }
        if (edgesNumber > verticesNumber * verticesNumber) {
            throw new RuntimeException("Too many edges");
        }
        this.verticesNumber = verticesNumber;
        this.edgesNumber = edgesNumber;
        this.adjacentMatrix = new DirectedEdge[verticesNumber][edgesNumber];
        /*// can be inefficient
        while (this.edgesNumber != edgesNumber) {
            int v = (int) (verticesNumber * Math.random());
            int w = (int) (verticesNumber * Math.random());
            double weight = Math.round(100 * Math.random()) / 100.0;
            addEdge(new DirectedEdge(v, w, weight));
        }*/
    }

    // number of vertices and edges
    public int getVerticesNumber() {
        return verticesNumber;
    }

    public int getEdgesNumber() {
        return edgesNumber;
    }

    // add directed edge v->w
    public void addEdge(DirectedEdge e) {
        int v = e.from();
        int w = e.to();
        //Logger.printMessage("TRACE: " + adjacentMatrix.length + " " + v + " " + w + " " + adjacentMatrix[v][w]);
        if (adjacentMatrix[v][w] == null) {
            //edgesNumber++;
            adjacentMatrix[v][w] = e;
        }
    }

    // return list of neighbors of v
    public Iterable<DirectedEdge> adj(int v) {
        return new AdjIterator(v);
    }

    // support iteration over graph vertices
    private class AdjIterator implements Iterator<DirectedEdge>, Iterable<DirectedEdge> {

        private int v;
        private int w = 0;

        public AdjIterator(int v) {
            this.v = v;
        }

        public Iterator<DirectedEdge> iterator() {
            return this;
        }

        public boolean hasNext() {
            while (w < verticesNumber) {
                if (adjacentMatrix[v][w] != null) {
                    return true;
                }
                w++;
            }
            return false;
        }

        public DirectedEdge next() {
            if (hasNext()) {
                return adjacentMatrix[v][w++];
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    // string representation of Graph - takes quadratic time
    public String toString() {
        final String NEWLINE = System.getProperty("line.separator");
        StringBuilder s = new StringBuilder();
        s.append(verticesNumber).append(" ").append(edgesNumber).append(NEWLINE);
        for (int v = 0; v < verticesNumber; v++) {
            s.append(v).append(": ");
            for (DirectedEdge e : adj(v)) {
                s.append(e).append("  ");
            }
            s.append(NEWLINE);
        }
        return s.toString();
    }

    // test client
    public static void main(String[] args) {
        int V = Integer.parseInt(args[0]);
        int E = Integer.parseInt(args[1]);
        AdjMatrixEdgeWeightedDigraph G = new AdjMatrixEdgeWeightedDigraph(V, E);
        Logger.printMessage("Graph: " + G);
    }
}