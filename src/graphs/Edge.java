package graphs;

/**
 * Created by Chernyshov Yuriy on 8/8/13
 */
public class Edge {

    private final String id;
    private final Vertex source;
    private final Vertex destination;
    private final int weight;
    private boolean isInUnion;

    public Edge(String id, Vertex source, Vertex destination, int weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
        isInUnion = Boolean.FALSE;
    }

    public boolean isInUnion() {
        return isInUnion;
    }

    public void setInUnion(boolean inUnion) {
        isInUnion = inUnion;
    }

    public String getId() {
        return id;
    }
    public Vertex getDestination() {
        return destination;
    }

    public Vertex getSource() {
        return source;
    }
    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }
}