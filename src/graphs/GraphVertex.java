package graphs;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 7/27/13
 * Time: 3:05 PM
 */
public class GraphVertex {

    private int label;
    private int finishingTime;
    private int leader;
    private boolean isDiscovered;
    private boolean isExplored;
    private String type = "";
    private LinkedList<Integer> points;
    private LinkedList<Integer> reversePoints;
    private LinkedList<Integer> removedPoints;

    public GraphVertex() {
        points = new LinkedList<Integer>();
        removedPoints = new LinkedList<Integer>();
        reversePoints = new LinkedList<Integer>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLeader() {
        return leader;
    }

    public void setLeader(int leader) {
        this.leader = leader;
    }

    public boolean isExplored() {
        return isExplored;
    }

    public void setExplored(boolean explored) {
        isExplored = explored;
    }

    public boolean isDiscovered() {
        return isDiscovered;
    }

    public void setDiscovered(boolean discovered) {
        isDiscovered = discovered;
    }

    public int getFinishingTime() {
        return finishingTime;
    }

    public void setFinishingTime(int finishingTime) {
        this.finishingTime = finishingTime;
    }

    public int getLabel() {
        return label;
    }

    public String getLabelToString() {
        return String.valueOf(label);
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public LinkedList<Integer> getPoints() {
        return points;
    }

    public void addPoint(int point) {
        points.add(point);
    }

    public void addReversePoint(int point) {
        reversePoints.add(point);
    }

    public void addPointAtLast(int point) {
        points.addLast(point);
    }

    public void addReversePointAtLast(int point) {
        reversePoints.addLast(point);
    }

    public int getPointsSize() {
        return points.size();
    }

    public int getReversedPointsSize() {
        return reversePoints.size();
    }

    public int getReversedPointAt(int position) {
        return reversePoints.get(position);
    }

    public int getRemovedPointsSize() {
        return removedPoints.size();
    }

    public int getPointAt(int position) {
        return points.get(position);
    }

    public int removePointAt(int position) {
        return points.remove(position);
    }

    public LinkedList<Integer> getRemovedPoints() {
        return removedPoints;
    }

    public int getRemovedPointAt(int value) {
        return removedPoints.get(value);
    }

    public void addRemovedPoint(int graphVertex) {
        removedPoints.add(graphVertex);
    }

    public void addRemovedPointAtLast(int graphVertex) {
        removedPoints.addLast(graphVertex);
    }

    public int getPointPositionValue(int pointPosition) {
        return points.get(pointPosition);
    }
}