package graphs;

import utils.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 10/4/13
 * Time: 10:18 AM
 */
public class DirectedEdge {

    private final int v;
    private final int w;
    private final double weight;

    /**
     * Create a directed edge from v to w with given weight.
     */
    public DirectedEdge(int v, int w, double weight) {
        this.v = v;
        this.w = w;
        this.weight = weight;
    }

    /**
     * Return the vertex where this edge begins.
     */
    public int from() {
        return v;
    }

    /**
     * Return the vertex where this edge ends.
     */
    public int to() {
        return w;
    }

    /**
     * Return the weight of this edge.
     */
    public double weight() {
        return weight;
    }

    /**
     * Return a string representation of this edge.
     */
    public String toString() {
        return v + "->" + w + " " + String.format("%5.2f", weight);
    }

    /**
     * Test client.
     */
    public static void main(String[] args) {
        DirectedEdge e = new DirectedEdge(12, 23, 3.14);
        Logger.printMessage("Directed edge: " + e);
    }
}