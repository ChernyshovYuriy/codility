package graphs;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 7/27/13
 * Time: 3:07 PM
 */
public class GraphManager {

    public static int finishingTime = 1;
    public static int leader;

    public GraphManager() {

    }

    public static int cut(LinkedList<GraphVertex> graphVertexes) {
        if (graphVertexes.size() == 2) {
            return graphVertexes.get(0).getPoints().size();
        }

        // Get random vertex
        int nextRandomVertexPosition = (int) (Math.random() * (graphVertexes.size() - 1));
        GraphVertex graphVertex_A = graphVertexes.get(nextRandomVertexPosition);

        // At random selected vertex get random Point
        int nextRandomPointPosition = (int) (Math.random() * (graphVertex_A.getPointsSize() - 1));
        System.out.println(graphVertex_A.getPointsSize());
        int nextRandomPointPositionValue = graphVertex_A.getPointPositionValue(nextRandomPointPosition);

        GraphVertex graphVertex_B = null;
        for (int i = 0; i < graphVertexes.size(); i++) {
            if (graphVertexes.get(i).getRemovedPoints().contains(nextRandomPointPositionValue)) {
                graphVertex_B = graphVertexes.get(i);
                break;
            }
        }

        // Merge graphVertex_A with graphVertex_B
        for (int i = 0; i < graphVertex_B.getPointsSize(); i++) {
            graphVertex_A.addPointAtLast(graphVertex_B.getPointAt(i));
        }
        for (int i = 0; i < graphVertex_B.getRemovedPointsSize(); i++) {
            graphVertex_A.addRemovedPointAtLast(graphVertex_B.getRemovedPointAt(i));
        }

        for (int i = 0; i < graphVertex_A.getPointsSize(); i++) {
            if (graphVertex_A.getRemovedPoints().contains(graphVertex_A.getPointAt(i))) {
                graphVertex_A.removePointAt(i);
                i--;
            }
        }

        // Remove graphVertex_B
        graphVertexes.remove(graphVertex_B);

        return cut(graphVertexes);
    }

    public static void resetGraphDiscoveredExplored(Dictionary<Integer, GraphVertex> graphVertexes) {
        Integer key;
        GraphVertex value;
        Enumeration<Integer> keys = graphVertexes.keys();
        while (keys.hasMoreElements()) {
            key = keys.nextElement();
            value = graphVertexes.get(key);
            value.setExplored(Boolean.FALSE);
            value.setDiscovered(Boolean.FALSE);
        }
    }

    public static void DFS_Recursive(Dictionary<Integer, GraphVertex> graph, GraphVertex vertex,
                                     String order, boolean doProcessLeader, boolean doProcessFinishingTime) {
        if (vertex.isExplored()) {
            return;
        }
        vertex.setExplored(Boolean.TRUE);
        System.out.println("Vertex " + vertex.getLabel());
        GraphVertex edge;
        for (int i = 0; i < vertex.getReversedPointsSize(); i++) {
            edge = graph.get(vertex.getReversedPointAt(i));
            if (!edge.isExplored()) {
                DFS_Recursive(graph, edge, order, doProcessLeader, doProcessFinishingTime);
            }
        }
    }

    public static Collection<GraphVertex> DFS_Iterative(Dictionary<Integer, GraphVertex> graph, GraphVertex initVertex,
                                                   String order, boolean doProcessLeader, boolean doProcessFinishingTime) {
        Stack<GraphVertex> stack = new Stack<GraphVertex>();
        Collection<GraphVertex> returnCollection = null;
        if (doProcessLeader) {
            returnCollection = new LinkedList<GraphVertex>();
        }
        GraphVertex currentVertex;
        GraphVertex edge;
        initVertex.setExplored(Boolean.TRUE);
        stack.push(initVertex);
        int leader = initVertex.getLabel();
        //System.out.println(" --- " + leader);
        mainLoop: while (!stack.isEmpty()) {
            currentVertex = stack.peek();
            /*System.out.println("vertex label:" + currentVertex.getLabel() + " finishing time:" +
                    currentVertex.getFinishingTime() + " " +
                    currentVertex.isDiscovered() + " " +
                    currentVertex.isExplored());*/
            if (order.equalsIgnoreCase("reverse")) {
                for (int i = 0; i < currentVertex.getReversedPointsSize(); i++) {
                    edge = graph.get(currentVertex.getReversedPointAt(i));
                    //System.out.println("  edge " + edge.getLabel() + " " + edge.isDiscovered() + " " + edge.isExplored());
                    if (!edge.isExplored()) {
                        edge.setExplored(Boolean.TRUE);
                        stack.push(edge);
                        continue mainLoop;
                    }
                }
            } else {
                for (int i = 0; i < currentVertex.getPointsSize(); i++) {
                    edge = graph.get(currentVertex.getPointAt(i));
                    //System.out.println("  edge " + edge.getLabel() + " " + edge.isDiscovered() + " " + edge.isExplored());
                    if (!edge.isExplored()) {
                        edge.setExplored(Boolean.TRUE);
                        stack.push(edge);
                        continue mainLoop;
                    }
                }
            }
            currentVertex.setDiscovered(Boolean.TRUE);
            if (doProcessLeader) {
                currentVertex.setLeader(leader);
                returnCollection.add(currentVertex);
                //System.out.println("[I] Vertex: " + currentVertex.getLabel() + " Leader: " + leader);
            }
            if (doProcessFinishingTime) {
                currentVertex.setFinishingTime(finishingTime++);
                //System.out.println("[I] Vertex: " + currentVertex.getLabel() + " Finishing Time: " + currentVertex.getFinishingTime());
            }
            stack.pop();
        }

        return returnCollection;
    }
}