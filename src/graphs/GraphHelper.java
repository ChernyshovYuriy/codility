package graphs;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 8/4/13
 * Time: 4:44 PM
 */
public class GraphHelper {

    int maxLabelValue;
    int maxHeadValue;

    public int getMaxLabelValue() {
        return maxLabelValue;
    }

    public void setMaxLabelValue(int maxLabelValue) {
        this.maxLabelValue = maxLabelValue;
    }

    public int getMaxHeadValue() {
        return maxHeadValue;
    }

    public void setMaxHeadValue(int maxHeadValue) {
        this.maxHeadValue = maxHeadValue;
    }
}