package utils;

/**
 * Created by Chernyshov Yuriy on 03.09.13
 */
public class Logger {

    public static void printMessage(String message) {
        System.out.println("[I] " + message);
    }

    public static void printWarning(String message) {
        System.out.println("[W] " + message);
    }

    public static void printError(String message) {
        System.out.println("[E] " + message);
    }
}