package utils;

import algorithms.Job;
import algorithms.Knapsack;
import algorithms.KnapsackEntity;
import graphs.*;
import sorting.MaxSpaceClusteringComparator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 7/20/13
 * Time: 8:19 AM
 */
public class Utilities {

    public static String randomString(int stringLength) {
        Random r = new Random();
        String alphabet = "qwertyuiopasdfghjklzxcvbnm";
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < stringLength; i++) {
            result.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        return result.toString();
    }

    public static String intsToString(int[] ints) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int value : ints) {
            stringBuilder.append(String.valueOf(value));
            stringBuilder.append(",");
        }
        return stringBuilder.toString();
    }

    public static String longsToString(long[] longs) {
        StringBuilder stringBuilder = new StringBuilder();
        for (long value : longs) {
            stringBuilder.append(String.valueOf(value));
            stringBuilder.append(",");
        }
        return stringBuilder.toString();
    }

    public static int[] loadIntegerArray(String filePath) {
        ArrayList<Integer> integersList = loadIntegerArrayList(filePath);
        int size = integersList.size();
        int[] integers = new int[size];
        for (int i = 0; i < size; i++) {
            integers[i] = integersList.get(i);
        }
        return integers;
    }

    public static Double[] loadDoubleArray(String filePath) {
        ArrayList<Double> doubleArrayList = loadDoubleArrayList(filePath);
        int size = doubleArrayList.size();
        Double[] doubles = new Double[size];
        for (int i = 0; i < size; i++) {
            doubles[i] = doubleArrayList.get(i);
        }
        return doubles;
    }

    public static long[] loadLongArrayNoDuplicates(String filePath) {
        ArrayList<Long> list = loadLongArrayList(filePath);
        Set<Long> set = new TreeSet<>();
        for (long value : list) {
            set.add(value);
        }
        int counter = 0;
        long[] result = new long[set.size()];
        for (long value : set) {
            result[counter++] = value;
        }
        return result;
    }

    public static long[] loadLongArray(String filePath) {
        ArrayList<Long> list = loadLongArrayList(filePath);
        int size = list.size();
        long[] result = new long[size];
        for (int i = 0; i < size; i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public static ArrayList<Integer> loadIntegerArrayList(String filePath) {
        File file = new File(filePath);
        ArrayList<Integer> integers = new ArrayList<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                integers.add(Integer.parseInt(line));
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        System.out.println("Process is finished. Size: " + integers.size());
        return integers;
    }

    public static ArrayList<Double> loadDoubleArrayList(String filePath) {
        File file = new File(filePath);
        ArrayList<Double> doubles = new ArrayList<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                doubles.add(Double.parseDouble(line));
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        System.out.println("Process is finished. Size: " + doubles.size());
        return doubles;
    }

    public static ArrayList<Job> loadJobs(String filePath) {
        File file = new File(filePath);
        ArrayList<Job> result = new ArrayList<>(10000);
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                //result.add(Long.parseLong(line));
                String[] strings = line.split(" ");
                if (strings.length == 2) {
                    //Logger.printMessage(strings[0] + " " + strings[1]);
                    Job job = new Job();
                    job.setWeight(Integer.valueOf(strings[0]));
                    job.setLength(Integer.valueOf(strings[1]));

                    result.add(job);
                }
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        return result;
    }

    public static ArrayList<Long> loadLongArrayList(String filePath) {
        File file = new File(filePath);
        ArrayList<Long> result = new ArrayList<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.add(Long.parseLong(line));
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        System.out.println("Process is finished. Size: " + result.size());
        return result;
    }

    public static LinkedList<GraphVertex> loadGraphForMinCut(String filePath) {
        File file = new File(filePath);
        LinkedList<GraphVertex> graphVertexes = new LinkedList<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            GraphVertex graphVertex;
            String regExp = "\\s";
            while ((line = bufferedReader.readLine()) != null) {
                graphVertex = new GraphVertex();
                String[] strings = line.split(regExp);
                for (int j = 0; j < strings.length; j++) {
                    if (j == 0) {
                        graphVertex.addRemovedPointAtLast(Integer.valueOf(strings[j]));
                    } else {
                        graphVertex.addPointAtLast(Integer.valueOf(strings[j]));
                    }
                }
                graphVertexes.addLast(graphVertex);
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        return graphVertexes;
    }

    public static Hashtable<Integer, GraphVertex> loadGraphForSCC(String filePath) {
        File file = new File(filePath);
        Hashtable<Integer, GraphVertex> hashtable = new Hashtable<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String regExp = "\\s";
            int label;
            int head;

            GraphVertex graphVertex;
            String[] strings;

            while ((line = bufferedReader.readLine()) != null) {
                strings = line.split(regExp);
                label = Integer.valueOf(strings[0]);
                head = Integer.valueOf(strings[1]);

                // Directed
                graphVertex = hashtable.get(label);
                if (graphVertex == null) {
                    graphVertex = new GraphVertex();
                    graphVertex.setLabel(label);
                    hashtable.put(label, graphVertex);
                }
                graphVertex.addPointAtLast(head);

                // Reversed
                graphVertex = hashtable.get(head);
                if (graphVertex == null) {
                    graphVertex = new GraphVertex();
                    graphVertex.setLabel(head);
                    hashtable.put(head, graphVertex);
                }
                graphVertex.addReversePointAtLast(label);
            }

            System.out.println("Process Graph size: " + hashtable.size());
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }

        return hashtable;
    }

    public static Graph loadGraphForMSTAlgorithm(String filePath) {
        File file = new File(filePath);
        ArrayList<Vertex> nodes = new ArrayList<>();
        ArrayList<Edge> edges = new ArrayList<>();
        Vertex vertex;
        for (int i = 0; i < 500; i++) {
            vertex = new Vertex("" + i, "Vertex_" + i);
            nodes.add(vertex);
        }
        Edge edge;
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String[] dataArray;
            int counter = 0;

            while ((line = bufferedReader.readLine()) != null) {
                if (counter == 0) {
                    continue;
                }
                dataArray = line.split(" ");
                int node_A_index = Integer.valueOf(dataArray[0]) - 1;
                int node_B_index = Integer.valueOf(dataArray[1]) - 1;
                Logger.printMessage("A: " + node_A_index + " B: " + node_B_index);
                edge = new Edge("Edge_" + counter++, nodes.get(node_A_index), nodes.get(node_B_index),
                        Integer.valueOf(dataArray[2]));
                edges.add(edge);
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        return new Graph(nodes, edges);
    }

    public static Graph loadGraphForMaxSpaceClustering(String filePath) {
        File file = new File(filePath);
        ArrayList<Vertex> nodes = new ArrayList<>();
        ArrayList<Edge> edges = new ArrayList<>();
        Vertex vertex;
        for (int i = 0; i < 500; i++) {
            vertex = new Vertex("" + i, "" + i);
            nodes.add(vertex);
        }
        Edge edge;
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String[] dataArray;
            int counter = -1;

            while ((line = bufferedReader.readLine()) != null) {
                counter += 1;
                if (counter == 0) {
                    continue;
                }
                dataArray = line.split(" ");
                int node_A_index = Integer.valueOf(dataArray[0]) - 1;
                int node_B_index = Integer.valueOf(dataArray[1]) - 1;
                //Logger.printMessage("A: " + node_A_index + " B: " + node_B_index);
                edge = new Edge("" + counter, nodes.get(node_A_index), nodes.get(node_B_index),
                        Integer.valueOf(dataArray[2]));
                edges.add(edge);
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        Collections.sort(edges, new MaxSpaceClusteringComparator());
        return new Graph(nodes, edges);
    }

    public static AdjMatrixEdgeWeightedDigraph loadAllPairsShortestPathData(String filePath) {
        File file = new File(filePath);
        AdjMatrixEdgeWeightedDigraph digraph = null;

        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String[] dataArray;
            int counter = -1;

            while ((line = bufferedReader.readLine()) != null) {
                counter += 1;
                dataArray = line.split(" ");
                if (counter == 0) {
                    int verticesNumber = Integer.valueOf(dataArray[0]) + 1;
                    int edgesNumber = Integer.parseInt(dataArray[1]) + 1;
                    Logger.printMessage("Vertices N: " + verticesNumber + ", Edges N: " + edgesNumber);
                    digraph = new AdjMatrixEdgeWeightedDigraph(verticesNumber, edgesNumber);
                    continue;
                }
                if (digraph != null) {
                    digraph.addEdge(new DirectedEdge(Integer.valueOf(dataArray[0]), Integer.valueOf(dataArray[1]),
                            Integer.valueOf(dataArray[2])));
                }
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }

        return digraph;
    }

    public static Knapsack loadKnapsackData(String filePath) {
        File file = new File(filePath);
        Knapsack knapsack = new Knapsack();
        List<KnapsackEntity> result = new ArrayList<>();
        KnapsackEntity entity;
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String[] dataArray;
            int counter = -1;

            while ((line = bufferedReader.readLine()) != null) {
                counter += 1;
                dataArray = line.split(" ");
                if (counter == 0) {
                    knapsack.setCapacity(Integer.valueOf(dataArray[0]));
                    continue;
                }
                entity = new KnapsackEntity();
                entity.setValue(Integer.valueOf(dataArray[0]));
                entity.setWeight(Integer.valueOf(dataArray[1]));

                result.add(entity);
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }
        knapsack.setData(result);
        return knapsack;
    }

    public static Graph loadGraphForDijkstra(String filePath) {
        File file = new File(filePath);
        ArrayList<Vertex> nodes = new ArrayList<>();
        ArrayList<Edge> edges = new ArrayList<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String regExp = "\\s";

            Vertex location;
            Edge lane;
            String[] dataArray;
            String[] edgesArray;
            int counter = 0;
            List<String[]> list = new ArrayList<String[]>();

            while ((line = bufferedReader.readLine()) != null) {
                list.add(line.split(regExp));
            }

            for (String[] aList : list) {
                dataArray = aList;
                location = new Vertex(dataArray[0], "Vertex_" + dataArray[0]);
                nodes.add(location);
            }

            for (String[] aList : list) {
                dataArray = aList;
                for (int i = 0; i < dataArray.length; i++) {
                    if (i > 0) {
                        edgesArray = dataArray[i].split(",");
                        lane = new Edge("Edge_" + counter++, nodes.get(Integer.valueOf(dataArray[0]) - 1),
                                nodes.get(Integer.valueOf(edgesArray[0]) - 1), Integer.valueOf(edgesArray[1]));
                        edges.add(lane);
                    }
                }
            }

            System.out.println("Process Graph. Vertexes size: " + nodes.size() + ", Edges size: " + edges.size());
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }

        return new Graph(nodes, edges);
    }

    public static int[] getDataForMedianMaintenance(String filePath) {
        File file = new File(filePath);
        int[] data = new int[10000];
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            int counter = 0;
            while ((line = bufferedReader.readLine()) != null) {
                data[counter++] = Integer.valueOf(line);
            }

            System.out.println("Process data complete, size: " + data.length);
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        }

        return data;
    }
}