package sorting;

import graphs.Edge;

import java.util.Comparator;

/**
 * Created by ChernyshovYuriy on 16.09.13
 */
public class MaxSpaceClusteringComparator implements Comparator<Edge> {

    @Override
    public int compare(Edge o1, Edge o2) {
        if (o1.getWeight() > o2.getWeight()) {
            return 1;
        } else if (o1.getWeight() < o2.getWeight()){
            return -1;
        }
        return 0;
    }
}