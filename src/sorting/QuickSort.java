package sorting;

import java.util.Arrays;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 7/10/13
 * Time: 9:53 PM
 */
public final class QuickSort {

    public static void main(final String[] args) {
        final int[] data = {5, 8, 3, 6, 0, 1, 4, 2, 9, 7};
//        int[] data = utils.Utilities.loadIntegerArray("assets/IntegerArray.txt");
        System.out.println("Init   Array: " + Arrays.toString(data));
        final long startTime = System.currentTimeMillis();

        sort(data, 0, data.length  -1);

        System.out.println("Sorted Array: " + Arrays.toString(data));
        System.out.println("Running time: " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public static void sort(final int[] data, final int start, final int end) {

        if (start > end) {
            return;
        }

        // Case 1:

        // Case: 2
        /*temp = data[end];
        data[end] = data[start];
        data[start] = temp;*/

        // Case: 3
        /*int center = start + ((end - start) >> 1);
        int first = data[start];
        int middle = data[center];
        int last = data[end];
        int medianIndex = 0;
        if ((first > middle && first < last) || (first > last && first < middle)) {
            medianIndex = start;
        }
        if ((middle > first && middle < last) || (middle > last && middle < first)) {
            medianIndex = center;
        }
        if ((last > middle && last < first) || (last > first && last < middle)) {
            medianIndex = end;
        }
        if (medianIndex != start) {
            temp = data[medianIndex];
            data[medianIndex] = data[start];
            data[start] = temp;
        }*/

        final int randomIndex = getRandomIndex(start, end);
        swap(data, randomIndex, end);

        final int pivot = data[start];
        int split = start;

        for (int j = start + 1; j <= end; j++) {
            if (data[j] >= pivot) {
                continue;
            }
            split++;
            swap(data, split, j);
        }

        swap(data, split, start);

        sort(data, start, split - 1);
        sort(data, split + 1, end);
    }

    public static void sort(final long[] data, final int start, final int end) {

        if (end - start <= 0) {
            return;
        }

        // Case: 1
        Long temp;

        // Case: 2
        /*temp = data[end];
        data[end] = data[start];
        data[start] = temp;*/

        // Case: 3
        /*int center = start + ((end - start) >> 1);
        int first = data[start];
        int middle = data[center];
        int last = data[end];
        int medianIndex = 0;
        if ((first > middle && first < last) || (first > last && first < middle)) {
            medianIndex = start;
        }
        if ((middle > first && middle < last) || (middle > last && middle < first)) {
            medianIndex = center;
        }
        if ((last > middle && last < first) || (last > first && last < middle)) {
            medianIndex = end;
        }
        //System.out.println("TRACE " + data[start] + " " + data[center] + " " + data[end] + " " + data[medianIndex]);
        if (medianIndex != start) {
            temp = data[medianIndex];
            data[medianIndex] = data[start];
            data[start] = temp;
        }*/

        long pivot = data[start];

        int split = start;
        for (int j = start + 1; j <= end; j++) {
            if (data[j] >= pivot) {
                continue;
            }
            split++;
            temp = data[j];
            data[j] = data[split];
            data[split] = temp;
        }

        temp = data[split];
        data[split] = data[start];
        data[start] = temp;

        sort(data, start, split - 1);
        sort(data, split + 1, end);
    }

    private static int getRandomIndex(final int min, final int max) {
        final int limit = (max - min + 1);
        return new Random().nextInt(limit) + min;
    }

    private static void swap(final int[] data, final int position1, final int position2) {
        final int tmp = data[position1];
        data[position1] = data[position2];
        data[position2] = tmp;
    }
}