package sorting;

import java.util.Arrays;

public final class MergeSort {

    public static void main(final String[] args) {
        final int[] data = {5, 8, 3, 6, 0, 1, 4, 2, 9, 7};
//        int[] data = utils.Utilities.loadIntegerArray("assets/IntegerArray.txt");
        System.out.println("Init   Array: " + Arrays.toString(data));
        final long startTime = System.currentTimeMillis();

        sort(data);

        System.out.println("Sorted Array: " + Arrays.toString(data));
        System.out.println("Running time: " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public static void sort(final int[] data) {
        final int[] aux = new int[data.length];
        sort(data, aux, 0, data.length - 1);
    }

    private static void sort(final int[] data, final int[] aux,
                             final int lo, final int hi) {
        if (hi <= lo) {
            return;
        }
        final int mid = lo + ((hi - lo) >> 1);
        sort(data, aux, lo, mid);
        sort(data, aux, mid + 1, hi);
        merge(data, aux, lo, mid, hi);
    }

    private static void merge(final int[] data, final int[] aux,
                              final int lo, final int mid, final int hi) {
        System.arraycopy(data, lo, aux, lo, hi - lo + 1);

        int i = lo;
        int j = mid + 1;
        for (int k = lo; k <= hi; k++) {
            if (i > mid) {
                data[k] = aux[j++];
            } else if (j > hi) {
                data[k] = aux[i++];
            } else if (aux[j] < aux[i]) {
                data[k] = aux[j++];
            } else {
                data[k] = aux[i++];
            }
        }
    }
}
