package sorting;

import algorithms.Job;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 9/5/13
 * Time: 8:35 PM
 */
public class JobsDifferenceComparator implements Comparator<Job> {

    @Override
    public int compare(Job job_A, Job job_B) {
        int result = 0;
        if (job_A.getDifferenceFactor() < job_B.getDifferenceFactor()) {
            result = 1;
        } else if (job_A.getDifferenceFactor() > job_B.getDifferenceFactor()) {
            result = -1;
        } else {
            if (job_A.getWeight() < job_B.getWeight()) {
                result = 1;
            } else if (job_A.getWeight() > job_B.getWeight()) {
                result = -1;
            }
        }
        return result;
    }
}