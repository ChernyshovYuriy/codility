package sorting;

import algorithms.Job;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 9/5/13
 * Time: 9:25 PM
 */
public class JobsRatioComparator implements Comparator<Job> {

    @Override
    public int compare(Job job_A, Job job_B) {
        int result = 0;
        if (job_A.getRatioFactor() < job_B.getRatioFactor()) {
            result = 1;
        } else if (job_A.getRatioFactor() > job_B.getRatioFactor()) {
            result = -1;
        }
        return result;
    }
}