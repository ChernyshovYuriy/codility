package leetcode;

import java.util.*;

/**
 * Created by Chernyshov Yurii
 * At Intellij IDEA
 * On 28/12/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public class SumProblems {

    public static void main(final String[] args) {
        final int[] nums = new int[]{2, 7, 11, 15};
//        final int[] numsForThreeSum = new int[]{-1, 0, 1, 2, -1, -4};
//        final int[] numsForThreeSum = new int[]{-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6};
        final int[] numsForThreeSum = new int[]{-1,0,1,2,-1,-4};
        final int target = 9;

//        final int[] result = twoSum(nums, target);
//        if (result.length == 2) {
//            System.out.println(result[0] + " " + result[1]);
//        }

        final List<List<Integer>> resultForThreeSum = threeSum(numsForThreeSum);
        System.out.println(resultForThreeSum);
    }

    private static List<List<Integer>> threeSum(int[] nums) {
        if (nums == null || nums.length < 3) {
            return new ArrayList<>();
        }
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i + 2 < nums.length; i++) {
            int target = -nums[i];
            int front = i + 1;
            int back = nums.length - 1;

            while (front < back) {
                int sum = nums[front] + nums[back];

                // Finding answer which start from number nums[i]
                if (sum < target)
                    front++;

                else if (sum > target)
                    back--;

                else {
                    List<Integer> triplet = new ArrayList<>();
                    triplet.add(nums[i]);
                    triplet.add(nums[front]);
                    triplet.add(nums[back]);
                    res.add(triplet);

                    // Processing duplicates of Number 2
                    // Rolling the front pointer to the next different number forwards
                    while (front < back && nums[front] == triplet.get(1)) front++;

                    // Processing duplicates of Number 3
                    // Rolling the back pointer to the next different number backwards
                    while (front < back && nums[back] == triplet.get(2)) back--;
                }
            }

            // Processing duplicates of Number 1
            while (i + 1 < nums.length && nums[i + 1] == nums[i]) {
                i++;
            }
        }
        return res;
    }

    private static int[] twoSum(int[] nums, int target) {
        if (nums == null || nums.length == 2) {
            return new int[0];
        }
        final Map<Integer, Integer> map = new HashMap<>();
        final int length = nums.length;

        for (int i = 0; i < length; ++i) {
            if (map.containsKey(nums[i])) {
                final int[] result = new int[2];
                result[0] = map.get(nums[i]);
                result[1] = i;
                return result;
            } else {
                map.put(target - nums[i], i);
            }
        }
        return new int[0];
    }
}
