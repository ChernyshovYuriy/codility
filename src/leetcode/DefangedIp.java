package leetcode;

public class DefangedIp {

    public static void main(final String[] args) {
        DefangedIp defangedIp = new DefangedIp();
        final String ip = "255.100.50.0";
        final String defIp = defangedIp.defangIPaddr(ip);
        System.out.println(defIp);
    }

    private static final char SYMBOL = '.';
    private static final String REPLACE = "[.]";

    public String defangIPaddr(final String address) {
        final StringBuilder stringBuilder = new StringBuilder();
        final char[] chars = address.toCharArray();
        for (final char c : chars) {
            if (c == SYMBOL) {
                stringBuilder.append(REPLACE);
            } else {
                stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }
}
