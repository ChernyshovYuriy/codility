import utils.Logger;

import java.util.HashSet;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 1/10/15
 * Time: 5:48 PM
 */
public class Chlorum2014 {

    public static void main(String[] args) {
        new Chlorum2014();
    }

    public Chlorum2014() {
        int[] C = new int[]{1, 3, 0, 3, 2, 4, 4};
        int[] D = new int[]{6, 2, 7, 5, 6, 5, 2};
        int K = 5;

        Logger.printMessage("Result:" + solution(K, C, D));
    }

    public int solution(int K, int[] C, int[] D) {
        int returnValue = 0;

        final int length = C.length;
        final Node[] nodes = new Node[length];
        Node node;
        Node neighbourNode;
        int maxRateCityIndex = 0;
        int maxRateIndex = 0;
        int currentCityId;
        int neighbourCityId;
        int currentCityRate;

        for (int i = 0; i < length; i++) {
            currentCityId = i;
            neighbourCityId = C[i];
            currentCityRate = D[i];

            node = nodes[currentCityId];
            if (node == null) {
                node = new Node();
            }
            node.rate = currentCityRate;
            node.id = i;

            if (currentCityId != neighbourCityId) {
                node.neighbours.add(neighbourCityId);

                neighbourNode = nodes[neighbourCityId];
                if (neighbourNode == null) {
                    neighbourNode = new Node();
                }
                neighbourNode.neighbours.add(currentCityId);

                nodes[neighbourCityId] = neighbourNode;
            }

            if (currentCityRate >= maxRateIndex) {
                maxRateIndex = currentCityRate;
                maxRateCityIndex = i;
            }

            nodes[i] = node;
        }

        for (int i = 0; i < length; i++) {
            Logger.printMessage(nodes[i].toString());
        }

        final LinkedList<Node> queue = new LinkedList<Node>();

        node = nodes[maxRateCityIndex];
        node.isVisited = true;
        queue.add(node);
        returnValue++;
        Logger.printMessage("Selected node:" + node);

        while (!queue.isEmpty()) {

            node = queue.remove();

            for (int neighbourNodesId : node.neighbours) {
                neighbourNode = nodes[neighbourNodesId];

                if (neighbourNode.isVisited) {
                    continue;
                }

                currentCityRate = neighbourNode.rate;

                if (currentCityRate < 0) {
                    continue;
                }

                returnValue++;
                queue.add(neighbourNode);

                Logger.printMessage("Selected node:" + neighbourNode);

                neighbourNode.isVisited = true;

                if (returnValue >= K) {
                    queue.clear();
                    break;
                }
            }

        }

        int maxNonVisitedRate = 0;
        for (Node node1 : nodes) {
            if (node1.isVisited) {
                continue;
            }
            if (node1.rate > maxNonVisitedRate) {
                maxNonVisitedRate = node1.rate;
            }
        }

        Logger.printMessage("max non visited:" + maxNonVisitedRate);

        for (Node node1 : nodes) {
            if (!node1.isVisited) {
                continue;
            }
            if (node1.rate < maxNonVisitedRate) {
                returnValue--;
            }
        }

        return returnValue;
    }

    static class Node {

        int id;
        int rate;
        boolean isVisited = false;
        HashSet<Integer> neighbours = new HashSet<Integer>();

        @Override
        public String toString() {
            return "Node{" +
                    "id=" + id +
                    ", rate=" + rate +
                    ", isVisited=" + isVisited +
                    ", neighbours=" + neighbours +
                    '}';
        }
    }
}
