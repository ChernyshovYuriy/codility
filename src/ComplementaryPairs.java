/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 10:53 PM
 */
public class ComplementaryPairs {

    static int complementary_pairs (int K, int[] A) {
        int count = 0;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                if (A[j] + A[i] == K) {
                    count++;
                }
            }
        }
        System.out.println("ComplementaryPairs is " + count);
        return count;
    }
}