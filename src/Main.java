import OOPExercises.MyCircle;
import algorithms.*;
import collections.*;
import graphs.AdjMatrixEdgeWeightedDigraph;
import graphs.DirectedEdge;
import graphs.Graph;
import graphs.Vertex;
import sorting.JobsRatioComparator;
import sorting.QuickSort;
import tree.binary.BST_2sumFinder;
import tree.binary.BinaryTree;
import tree.binary.BinaryTreeEntity;
import tree.binary.CommonNodeFinder;
import utils.Logger;
import utils.StdOut;
import utils.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {


    private class A {

        public void print() {
            Logger.printMessage("A");
        }
    }

    private class B extends A {

        public void print() {
            Logger.printMessage("B");
        }
    }

    public static void main(int[] args) {

    }

    public static void main(String[] args) {
        System.out.println("Hello Codility");

        new Main();
    }

    public Main() {

        Map m;


        //A classD = (B) new A();
        //classD.print();

        //int[] data = new int[]{3, 5, 7, 3, 3, 5};
        //PrefixSuffixSet prefixSuffixSet = new PrefixSuffixSet();
        //int result = prefixSuffixSet.solution(data);
        //Logger.printMessage("Solution:" + result);

        //PrefixTree prefixTree = new PrefixTree();
        //prefixTree.insertString("hello");
        //prefixTree.insertString("health");
        //prefixTree.insertString("hellowen");
        //prefixTree.insertString("java");
        //prefixTree.insertString("codility");
        //prefixTree.insertString("data");
        //prefixTree.insertString("datas");
        //prefixTree.insertString("dummy");

        //prefixTree.printSorted(prefixTree.root, "hea");

        /*//434821,968,459,313,211
        System.out.println("Build Graph start ...");
        Hashtable<Integer, GraphVertex> graph = Utilities.loadGraphForSCC("assets/SCC.txt");
        System.out.println("Build Graph complete");

        System.out.println("Compute Finishing Times start ...");
        Integer key;
        GraphVertex vertex;
        Enumeration<Integer> keys = graph.keys();
        while (keys.hasMoreElements()) {
            key = keys.nextElement();
            vertex = graph.get(key);
            if (!vertex.isExplored() && !vertex.isDiscovered()) {
                GraphManager.DFS_Iterative(graph, vertex, "reverse", Boolean.FALSE, Boolean.TRUE);
            }
        }*/

        /*TreeMap<Integer, Integer> treeMap = new TreeMap<Integer, Integer>();
        for (Integer integer : graph.keySet()) {
            //System.out.println(integer);
            treeMap.put(graph.get(integer).getLabel(), graph.get(integer).getFinishingTime());
        }
        for (Integer integer : treeMap.keySet()) {
            System.out.println(integer + " " + treeMap.get(integer));
        }*/

        /*System.out.println("Compute Finishing times complete");

        System.out.println("Reset Graph start ...");
        GraphManager.resetGraphDiscoveredExplored(graph);
        System.out.println("Reset Graph complete");

        System.out.println("Compute Leaders start ...");
        keys = graph.keys();
        TreeMap<Integer, Integer> finishingTimesMap = new TreeMap<Integer, Integer>(Collections.reverseOrder());
        while (keys.hasMoreElements()) {
            key = keys.nextElement();
            vertex = graph.get(key);
            finishingTimesMap.put(vertex.getFinishingTime(), vertex.getLabel());
        }
        *//*System.out.println("Computing the new node order according to finishing times:");
        for(Map.Entry<Integer,Integer> entry : finishingTimesMap.entrySet()) {
            System.out.println(" - node:" + entry.getValue() + " finishing time: " + entry.getKey());
        }*//*
        TreeMap<Integer, Integer> groupSizes = new TreeMap<Integer, Integer>(Collections.reverseOrder());
        LinkedList<GraphVertex> group;
        for (Map.Entry<Integer,Integer> entry : finishingTimesMap.entrySet()) {
            vertex = graph.get(entry.getValue());

            if (!vertex.isExplored() && !vertex.isDiscovered()) {
                group = (LinkedList<GraphVertex>) GraphManager.DFS_Iterative(graph, vertex, "forward",
                        Boolean.TRUE, Boolean.FALSE);
                groupSizes.put(group.size(), vertex.getLeader());
            }
        }
        System.out.println("Compute Leaders complete");

        int counter = 0;
        for (Map.Entry<Integer,Integer> entry : groupSizes.entrySet()) {
            if (counter++ == 5) {
                break;
            }
            System.out.println("Group Size: " + entry.getKey() + ", leader: " + entry.getValue());
        }*/

        //computeDijkstasShortestPaths();

        //medianMaintenance();

        //compute2Sum();

        //computeCommonNode();

        //weightedCompletionTimesSum();

        //computeOverallMSTCost();

        //computeMaxSpacingClustering();

        //bst_2sumFinder();

        //analyzeLinkedList();

        //knapsackAlgorithm();

        //computeAllPairsShortestPathProblem();

        //oopExercises();

        /*try {
            // http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
            solveTravelSalesmanProblem();
        } catch (IOException e) {
            Logger.printError(e.toString());
        }*/

        /*IntList node_A = new IntList();
        IntList node_B = new IntList();
        IntList node_C = new IntList();
        IntList node_D = new IntList();

        node_A.value = 1;
        node_A.next = node_B;
        node_B.value = 2;
        node_B.next = node_C;
        node_C.value = 3;
        node_C.next = node_D;
        node_D.value = 4;
        node_D.next = null;

        IntList node;
        ArrayList<IntList> arrayList = new ArrayList<IntList>();
        for (int i = 0; i < 50000; i++) {
            node = new IntList();
            node.value = i;
            node.next = null;
            arrayList.add(node);
        }
        for (int i = 1; i < arrayList.size(); i++) {
            arrayList.get(i - 1).next = arrayList.get(i);
        }

        Logger.printMessage("Size: " + solution_a(arrayList.get(0)));

        int[] data = new int[]{4,3,2,4,2,2,2,2,4,6};
        //data = new int[]{100,50,1,1,1};
        //Logger.printMessage("Leader: " + solution(data));*/

        /*int[] data = new int[]{-5,-3,-1,0,1,3,6};
        Logger.printMessage("Abs-distinct: " + absDistinct(data));*/

        //int inputN = 15;
        //Logger.printMessage("binary gap of " + inputN + " is " + Codility.binaryGap(inputN));

        /*int[] data = new int[]{-7,1,5,2,-4,3,0};
        data = new int[]{1,1,1};
        Logger.printMessage("Equilibrium " + Codility.equilibrium(data));*/

        /*int[] data = new int[]{3,4,3,2,3,-1,3,3};
        Logger.printMessage("Dominator: " + Codility.dominator(data));*/

        /*int[] data = new int[]{23171, 21015, 21123, 21366, 21013, 21367};
        data = new int[100000];
        for (int  i = 0; i < data.length; i++) {
            data[i] = MathUtilities.randInt(0, 1000000);
        }
        Logger.printMessage("Profit: " + Codility.maxProfit(data));*/

        //int[] data = new int[]{8,8,5,7,9,8,7,4,8};
        //Logger.printMessage("Stone wall: " + Codility.stoneWall(data));

        // "abababa" - 10
        // "babbbababbbb" - 12
        // "baaab" - 5
        // "aaa" - 4

        /*String s = "aaaaa";
        //s = Utilities.randomString(50000);
        //Long startTime = System.currentTimeMillis();
        //Logger.printMessage("Product: " + Codility.maximumProduct(s) + ", running time: " +
        //        (System.currentTimeMillis() - startTime) + " ms");
        //Logger.printMessage("Product: " + BoyerMoor.indexOf(s.toCharArray(), new char[]{"t".charAt(0)}));

        long result = 0;
        byte signal = 0x09;

        result |= formatSignal(signal, 58, 4);
        //Logger.printMessage("Payload: 0x" + Long.toHexString(result));

        byte a = 0x58;
        byte b = 0x59;
        String str = "cqcwq";
        byte[] bytes = str.getBytes();
        for (byte bt : bytes) {
            Logger.printMessage("byte: " + bt);
        }*/


        // 6, [4, 5, 8, 5, 1, 4, 6, 8, 7, 2, 2, 5] - 44
        // 2, [3, 5, 7, 6, 3] - 9
        // 2 [6, 7] - 3

        /*int[] A = {4, 5, 8, 5, 1, 4, 6, 8, 7, 2, 2, 5};
        int K = 6;

        int solution = BoundedSlice.solution(K, A);
        Logger.printMessage("Solution: " + solution);*/

        //int[] data = new int[]{4, 3, 2, 8};
        //Logger.printMessage("Array of products:" + Arrays.toString(arrayOfProducts(data)));

        /*int[] data = new int[]{5,3,6,3,4,2}; //3
        data = new int[]{4, 5, 8, 5, 1, 4, 6, 8}; // 7
        //data = new int[]{4, 5, 8, 5, 1, 4, 6}; // 6
        data = new int[]{8, 7, 6, 5, 1, 4, 6}; // 4
        //data = new int[]{8, 7, 6, 5, 1, 4, 1}; // 2
        //data = new int[]{8, 7, 6, 5, 4, 3, 2, 2}; // 1
        //data = new int[]{5,5};
        MonotonicPair monotonicPair = new MonotonicPair();
        int result = monotonicPair.solution(data);
        Logger.printMessage("Result:" + result);*/
    }

    private int[] arrayOfProducts(int[] inputData) {
        int length = inputData.length;
        int[] result = new int[length];
        // Get the products below the current index
        int p = 1;
        int i;
        for (i = 0; i < length; i++) {
            result[i] = p;
            p *= inputData[i];
        }
        // Get the products above the current index
        p = 1;
        for (i = length - 1; i >= 0; i--) {
            result[i] *= p;
            p *= inputData[i];
        }
        return result;
    }

    private long formatSignal(byte signal, int position, int length) {
        long positionedSignal = (long) signal;
        positionedSignal = positionedSignal << 64 - position - length;
        return positionedSignal;
    }


    public int solution(int[] A) {
        int dataSize = A.length;
        int leader = -1;
        if (dataSize < 1 && dataSize > 1000000) {
            return leader;
        }
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        Integer counter;
        int maxCounter = Integer.MIN_VALUE;
        int maxCounterKey = -1;
        for (int item : A) {
            if (item < 0 && item > 2147483647) {
                continue;
            }
            counter = map.get(item);
            if (counter == null) {
                counter = 0;
            }
            counter++;
            map.put(item, counter);
            if (counter > maxCounter) {
                maxCounter = counter;
                maxCounterKey = item;
            }
        }
        if (maxCounterKey != -1 && map.get(maxCounterKey) > dataSize >> 1) {
            leader = maxCounterKey;
        }
        return leader;
    }

    public int solution_a(IntList L) {
        /*if (L == null) {
            return 0;
        }
        return 1 + solution_a(L.next);*/
        int size = 0;
        IntList currentItem = L;
        while (currentItem != null) {
            currentItem = currentItem.next;
            size++;
        }
        return size;
    }

    private void solveTravelSalesmanProblem() throws IOException {
        // Final distance: 26442.730308954753

        TravelSalesmanProblem travelSalesmanProblem = new TravelSalesmanProblem();
        try {
            travelSalesmanProblem.readInput(new InputStreamReader(new FileInputStream(new File("assets/tsp.txt"))));
        } catch (IOException e) {

        }
        travelSalesmanProblem.solve();

        /*BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("assets/tsp.txt"))));
        String line;
        int counter = -1;
        City city;
        while ((line = bufferedReader.readLine()) != null) {
            counter++;
            if (counter == 0) {
                continue;
            } else {
                String[] data = line.split(" ");
                city = new City(Double.parseDouble(data[0]), Double.parseDouble(data[1]));
                TourManager.addCity(city);
            }
        }

        // Initialize population
        Population pop = new Population(500, true);
        System.out.println("Initial distance: " + pop.getFittest().getDistance());

        // Evolve population for 25 generations
        pop = GA.evolvePopulation(pop);
        for (int i = 0; i < 1000; i++) {
            pop = GA.evolvePopulation(pop);
        }

        // Print final results
        System.out.println("Finished");
        System.out.println("Final distance: " + pop.getFittest().getDistance());
        System.out.println("Solution:");
        System.out.println(pop.getFittest());*/

    }

    private boolean findNumbersWithSum(LinkedList<int[]> data, int sum) {
        int length = data.size();
        if (length < 1) {
            return false;
        }
        int ahead = length - 1;
        int behind = 0;
        while (ahead > behind) {
            int curSum = data.get(ahead)[0] + data.get(behind)[0];
            if (curSum == sum) {
                return true;
            } else if (curSum > sum) {
                ahead--;
            } else {
                behind++;
            }
        }
        return false;
    }

    private void oopExercises() {
        MyCircle circle = new MyCircle(0, 0, 10);
        Logger.printMessage(circle.toString());
    }

    private void computeAllPairsShortestPathProblem() {
        Logger.printMessage("All Pairs Shortest Path Problem Algorithm, start load data ...");
        String filePath = "assets/AllPairsShortestPathProblem/graph_3.txt";
        AdjMatrixEdgeWeightedDigraph digraph = Utilities.loadAllPairsShortestPathData(filePath);
        Logger.printMessage("Data loaded");
        //Logger.printMessage("Graph: " + digraph.getVerticesNumber() + " " + digraph.getEdgesNumber());

        FloydWarshall spt = new FloydWarshall(digraph);
        // print all-pairs shortest path distances
       /* StdOut.printf("     ");
        for (int v = 0; v < digraph.getVerticesNumber(); v++) {
            StdOut.printf("%6d ", v);
        }
        StdOut.println();
        for (int v = 0; v < digraph.getVerticesNumber(); v++) {
            StdOut.printf("%3d: ", v);
            for (int w = 0; w < digraph.getVerticesNumber(); w++) {
                if (spt.hasPath(v, w)) {
                    StdOut.printf("%6.2f ", spt.dist(v, w));
                } else {
                    StdOut.printf("   Inf ");
                }
            }
            StdOut.println();
        }*/

        if (spt.hasNegativeCycle()) {
            // print negative cycle
            StdOut.println("Negative cost cycle:");
            for (DirectedEdge e : spt.negativeCycle()) {
                StdOut.println(e);
            }
            StdOut.println();
        } else {
            // print all-pairs shortest paths
            double result;
            double minResult = Double.MAX_VALUE;
            for (int v = 0; v < digraph.getVerticesNumber(); v++) {
                for (int w = 0; w < digraph.getVerticesNumber(); w++) {
                    if (spt.hasPath(v, w)) {
                        //StdOut.printf("%d to %d (%5.2f)  ", v, w, spt.dist(v, w));
                        //Logger.printMessage("Process result");
                        result = 0;
                        for (DirectedEdge e : spt.path(v, w)) {
                            //StdOut.print(e + "  ");
                            result += e.weight();
                        }
                        if (result < minResult) {
                            minResult = result;
                        }
                        //StdOut.println();
                    } else {
                        //StdOut.printf("%d to %d          no path\n", v, w);
                    }
                }
            }
            Logger.printMessage("Result: " + minResult);
        }
    }

    private void knapsackAlgorithm() {
        Logger.printMessage("KnapsackAlgorithmManager Algorithm, start load data ...");
        String filePath = "assets/knapsack_big.txt";
        Knapsack knapsack = Utilities.loadKnapsackData(filePath);
        Logger.printMessage("Data loaded");
        KnapsackAlgorithmManager algorithmManager = new KnapsackAlgorithmManager();
        algorithmManager.execute(knapsack.getData(), knapsack.getCapacity());
    }

    private void commonValuesInArrays() {
        int[] first = new int[]{5, 4, 6, 7, 3, 6, 2, 6, 3, 7, 5};
        int[] second = new int[]{4, 6, 8, 9, 1, 2, 8, 4};
        ArraysAndStrings.findCommonValues_v1(first, second);
        ArraysAndStrings.findCommonValues_v2(first, second);
    }

    private void analyzeLinkedList() {
        LinkedListEntity<Integer> mHead = new LinkedListEntity<Integer>(1);
        LinkedListEntity<Integer> element_A = new LinkedListEntity<Integer>(2);
        LinkedListEntity<Integer> element_B = new LinkedListEntity<Integer>(3);
        LinkedListEntity<Integer> element_C = new LinkedListEntity<Integer>(4);
        LinkedListEntity<Integer> element_D = new LinkedListEntity<Integer>(5);
        LinkedListEntity<Integer> element_E = new LinkedListEntity<Integer>(6);

        element_A.setNextEntityPointer(element_B);
        element_B.setNextEntityPointer(element_C);
        element_C.setNextEntityPointer(element_D);
        element_D.setNextEntityPointer(element_E);

        mHead.setNextEntityPointer(element_A);

        // Loop
        element_E.setNextEntityPointer(element_B);

        LinkedListAnalyzer analyzer = new LinkedListAnalyzer();
        Logger.printMessage("Has List a loop: " + analyzer.hasLoop(mHead));
        if (analyzer.meetingNode(mHead) != null) {
            Logger.printMessage("Meeting node: " + analyzer.meetingNode(mHead).getEntityValue());
            Logger.printMessage("Entry node: " + analyzer.entryNodeOfLoop(mHead).getEntityValue());
        }
        int k = 2;
        Logger.printMessage("'" + k + "' element from tail is " + analyzer.findKthToTail(mHead, k));
    }

    private void bst_2sumFinder() {
        BinaryTreeEntity mRoot = new BinaryTreeEntity(4);
        BinaryTreeEntity element_A = new BinaryTreeEntity(0);
        BinaryTreeEntity element_B = new BinaryTreeEntity(1);
        BinaryTreeEntity element_C = new BinaryTreeEntity(2);
        BinaryTreeEntity element_D = new BinaryTreeEntity(3);
        BinaryTreeEntity element_E = new BinaryTreeEntity(5);
        BinaryTreeEntity element_F = new BinaryTreeEntity(6);
        BinaryTreeEntity element_G = new BinaryTreeEntity(7);

        mRoot.setLeft(element_C);
        mRoot.setRight(element_F);

        element_C.setLeft(element_B);
        element_C.setRight(element_D);

        element_B.setLeft(element_A);

        element_F.setLeft(element_E);
        element_F.setRight(element_G);

        BST_2sumFinder finder = new BST_2sumFinder();
        int sum = 13;
        boolean answer = finder.hasTwoNodes(mRoot, sum);
        Logger.printMessage("Has BST 2 nodes for the sum '" + sum + "' - " + answer);
    }

    private void computeMaxSpacingClustering() {
        System.out.println("Build Graph start ...");
        Graph graph = Utilities.loadGraphForMaxSpaceClustering("assets/clustering1.txt");
        /*for (Edge edge : graph.getEdges()) {
            Logger.printMessage("" + edge.getWeight());
        }*/
        Clustering clustering = new Clustering();
        clustering.setGraph(graph);
        clustering.init();
        //clustering.maxSpacingClustering(4);
        System.out.println("Build Graph complete");
    }

    private void computeOverallMSTCost() {
        System.out.println("Build Graph start ...");
        Graph graph = Utilities.loadGraphForMSTAlgorithm("assets/edges.txt");
        System.out.println("Build Graph complete");
    }

    private void weightedCompletionTimesSum() {
        ArrayList<Job> jobs = Utilities.loadJobs("assets/jobs.txt");
        Collections.sort(jobs, new JobsRatioComparator());

        long completionTimesSum = 0;
        long jobLengthsSum = 0;
        for (Job job : jobs) {
            //Logger.printMessage(job.toString());
            jobLengthsSum += job.getLength();
            completionTimesSum += job.getWeight() * jobLengthsSum;
        }
        Logger.printMessage("Sum: " + completionTimesSum);
    }

    private void computeCommonNode() {
        // Construct BTree - start -
        BinaryTreeEntity<Integer> mRoot = new BinaryTreeEntity<Integer>(7);
        BinaryTreeEntity<Integer> node_A = new BinaryTreeEntity<Integer>(9);
        BinaryTreeEntity<Integer> node_B = new BinaryTreeEntity<Integer>(11);
        BinaryTreeEntity<Integer> node_C = new BinaryTreeEntity<Integer>(14);
        BinaryTreeEntity<Integer> node_D = new BinaryTreeEntity<Integer>(15);
        BinaryTreeEntity<Integer> node_E = new BinaryTreeEntity<Integer>(3);
        BinaryTreeEntity<Integer> node_F = new BinaryTreeEntity<Integer>(12);

        mRoot.setLeft(node_E);
        mRoot.setRight(node_F);
        mRoot.getLeft().setLeft(new BinaryTreeEntity<Integer>(1));
        mRoot.getLeft().setRight(new BinaryTreeEntity<Integer>(6));
        mRoot.getLeft().getLeft().setLeft(new BinaryTreeEntity<Integer>(0));
        mRoot.getLeft().getLeft().setRight(new BinaryTreeEntity<Integer>(2));
        mRoot.getLeft().getRight().setLeft(new BinaryTreeEntity<Integer>(4));
        mRoot.getLeft().getRight().getLeft().setLeft(new BinaryTreeEntity<Integer>(5));
        mRoot.getRight().setLeft(node_A);
        mRoot.getRight().setRight(new BinaryTreeEntity<Integer>(13));
        mRoot.getRight().getLeft().setLeft(new BinaryTreeEntity<Integer>(8));
        mRoot.getRight().getLeft().setRight(node_B);
        mRoot.getRight().getLeft().getRight().setLeft(new BinaryTreeEntity<Integer>(10));
        mRoot.getRight().getRight().setRight(node_D);
        mRoot.getRight().getRight().getRight().setLeft(node_C);

        BinaryTree<Integer> binaryTree = new BinaryTree<Integer>(mRoot);
        // Construct BTree - end -

        CommonNodeFinder commonNodeFinder = new CommonNodeFinder();
        commonNodeFinder.setFirstNode(node_C);
        commonNodeFinder.setSecondNode(node_F);
        commonNodeFinder.computeCommonNodeRecursively(mRoot);
        commonNodeFinder.printCommonNode();
    }

    private void compute2Sum() {
        System.out.println("Calculate 2 Sum");
        long[] inputArray = Utilities.loadLongArray("assets/2sum.txt");
        QuickSort.sort(inputArray, 0, inputArray.length - 1);
        int loopSize = inputArray.length;
        Set<Long> set = new HashSet<Long>();
        long minValue;
        long maxValue;
        long currentValue;
        final long DELTA = 10000;
        int minValueIndex;
        int maxValueIndex;
        long result;
        for (long l : inputArray) {
            currentValue = l;
            minValue = -DELTA - currentValue;
            maxValue = DELTA - currentValue;
            //System.out.println("value " + currentValue + " min val " + minValue + " max val " + maxValue);
            minValueIndex = Search.binarySearchLongIterable(inputArray, minValue);
            maxValueIndex = Search.binarySearchLongIterable(inputArray, maxValue);
            for (int j = minValueIndex; j < maxValueIndex; j++) {
                result = currentValue + inputArray[j];
                set.add(result);
            }
        }
        System.out.println("Result: " + set.size());
    }

    private void medianMaintenance() {
        System.out.println("Median Maintenance");
        HeapMaxComparator heapMaxComparator = new HeapMaxComparator();
        HeapMinComparator heapMinComparator = new HeapMinComparator();
        Heap<Integer> heapMin = new Heap<Integer>(heapMaxComparator);
        Heap<Integer> heapMax = new Heap<Integer>(heapMinComparator);

        int[] data = Utilities.getDataForMedianMaintenance("assets/Median.txt");
        int counter = 0;
        int mediansSum = 0;
        for (int value : data) {
            counter++;
            if (counter == 1) {
                heapMin.enqueue(value);
            } else if (counter == 2) {
                if (value > heapMin.getHead()) {
                    heapMax.enqueue(value);
                } else {
                    heapMax.enqueue(heapMin.dequeue());
                    heapMin.enqueue(value);
                }
            } else {
                // Add next value to the appropriative heap
                if (value < heapMax.getHead()) {
                    heapMin.enqueue(value);
                } else {
                    heapMax.enqueue(value);
                }
                // Balance heaps
                if (heapMin.size() != heapMax.size()) {
                    if (heapMax.size() - heapMin.size() == 1) {
                        heapMin.enqueue(heapMax.dequeue());
                    }
                    while (heapMin.size() - heapMax.size() > 1) {
                        heapMax.enqueue(heapMin.dequeue());
                    }
                }
            }
            mediansSum += heapMin.getHead();
        }
        System.out.println("Sum: " + mediansSum + " Medians sum: " + mediansSum % data.length);
    }

    private void computeDijkstasShortestPaths() {
        System.out.println("Build Graph start ...");
        Graph graph = Utilities.loadGraphForDijkstra("assets/dijkstraData.txt");
        System.out.println("Build Graph complete");

        // Targets
        //2599,2610,2947,2052,2367,2399,2029,2442,2505,3068
        int[] targetVertexes = new int[]{7, 37, 59, 82, 99, 115, 133, 165, 188, 197};

        DijkstraAlgorithm algorithm = new DijkstraAlgorithm(graph);
        algorithm.execute(algorithm.getVertexAt(0));
        StringBuilder stringBuilder = new StringBuilder();
        int loopSize = targetVertexes.length;
        for (int i = 0; i < loopSize; i++) {
            LinkedList<Vertex> path = algorithm.getPath(algorithm.getVertexAt(targetVertexes[i] - 1));
            if (path == null) {
                System.out.println("Distance does not exists");
                continue;
            }
            int distance = 0;
            Vertex previousVertex = null;
            for (Vertex vertex : path) {
                //System.out.println(previousVertex + " " + vertex);
                if (previousVertex != null) {
                    distance += algorithm.getDistance(previousVertex, vertex);
                }
                previousVertex = vertex;
            }
            stringBuilder.append(String.valueOf(distance));
            if (i < loopSize - 1) {
                stringBuilder.append(",");
            }
        }
        System.out.println(stringBuilder.toString());
    }
}

class IntList {
    public int value;
    public IntList next;
}