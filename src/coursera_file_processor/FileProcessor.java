package coursera_file_processor;

import java.io.*;

/**
 * Created by Chernyshov Yuriy on 25.07.13.
 *
 */
public class FileProcessor {

    public FileProcessor() {

    }

    public void processFile(String filePath) {
        File file = new File(filePath);
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            while((line = bufferedReader.readLine()) != null) {
                //System.out.println("Line: " + line.endsWith("."));
                if (line.length() > 0) {
                    line = line.replaceAll("\\. ", "\\.\n");
                    stringBuilder.append(line);
                    if (line.endsWith(".")) {
                        stringBuilder.append("\n");
                    } else {
                        stringBuilder.append(" ");
                    }
                }
            }
            //System.out.println("Text:\n" + file.getName());
            filePath = file.getAbsolutePath().replaceAll(" ", "").replaceAll(".txt", "_modified.txt");
            System.out.println("File '" + filePath + "' processed");
            saveResultToFile(filePath, stringBuilder.toString());
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ignored) {

            }
        }
    }

    private void saveResultToFile(String filePath, String stringData) {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;

        try {
            fileWriter = new FileWriter(filePath);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(stringData);

            System.out.println("File '" + filePath + "' saved");
        } catch (IOException e) {
            System.out.println("Save result to file error: " + e);
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException ignored) {

            }
        }
    }
}