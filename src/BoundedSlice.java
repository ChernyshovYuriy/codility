import utils.Logger;

public class BoundedSlice {

    public static int solution(int K, int[] A) {
        int result = 0;
        if (K < 0 && K > 1000000) {
            return result;
        }
        int length = A.length;
        if (length > 100000) {
            return result;
        }
        int min;
        int max;
        int minRev;
        int maxRev;
        /*for (int i = 0; i < length; i++) {
            min = A[i];
            max = A[i];
            for (int j = i; j < length; j++) {
                if (A[j] > max) {
                    max = A[j];
                }
                if (A[j] < min) {
                    min = A[j];
                }
                if (max - min <= K) {
                    result++;
                    if (result > 1000000000) {
                        return 1000000000;
                    }
                }
            }
        }*/
        min = A[0];
        max = A[0];
        minRev = A[length - 1];
        maxRev = A[length - 1];
        result = length;
        int counter = 0;
        int stopPoint = length / 2;
        for (int i = 0; i < length; i++) {
            if (A[i] > max) {
                Logger.printMessage(" ↑:" + i);
                max = A[i];
            }
            if (A[i] < min) {
                Logger.printMessage(" ↓:" + i);
                min = A[i];
            }
            Logger.printMessage("Max:" + max + ", Min:" + min);
            if (max - min <= K) {
                result++;
                if (result > 1000000000) {
                    return 1000000000;
                }
            }
            if (i == 0) {
                continue;
            }
            if (A[length - i] > maxRev) {
                Logger.printMessage(" ↑Rev:" + i);
                maxRev = A[length - i];
            }
            if (A[length - i] < minRev) {
                Logger.printMessage(" ↓Rev:" + i);
                minRev = A[length - i];
            }
            Logger.printMessage("MaxRev:" + maxRev + ", MinRev:" + minRev);
            if (maxRev - minRev <= K) {
                result++;
                if (result > 1000000000) {
                    return 1000000000;
                }
            }
            if (i == length - 1) {
                if (counter <= stopPoint) {
                    if (counter == 0) {
                        result -= 2;
                    } else {
                        result -= 1;
                    }
                    length--;
                    counter++;
                    i = counter;

                    min = A[i];
                    max = A[i];
                    minRev = A[length - 1];
                    maxRev = A[length - 1];
                    Logger.printMessage(" ********** ");
                }
            }
        }
        //result -= 2;
        return result;
    }


}