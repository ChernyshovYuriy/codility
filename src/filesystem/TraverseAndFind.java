package filesystem;

import java.io.File;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Chernyshov Yuriy
 * Date: 17.05.13
 * Time: 13:00
 */
public class TraverseAndFind {

    public TraverseAndFind() {

    }

    public void startTraversing(String rootDirPath, int depthLevel, String searchPattern) {
        if (rootDirPath != null) {
            LinkedList<FileItem> queue = new LinkedList<FileItem>();

            FileItem fileItem = new FileItem();
            fileItem.setValue(new File(rootDirPath));
            fileItem.setDepth(0);
            queue.add(fileItem);

            int currentDepth;
            while(!queue.isEmpty()) {
                fileItem = queue.remove();
                currentDepth = fileItem.getDepth() + 1;
                if (depthLevel != -1 && currentDepth == depthLevel) {
                    continue;
                }
                File[] files = fileItem.getValue().listFiles();
                if (files != null) {
                    for (File child : files) {
                        //System.out.println(child.getName());
                        if (child.getName().contains(searchPattern)) {
                            System.out.println(child.getName());
                        }
                        fileItem = new FileItem();
                        fileItem.setValue(child);
                        fileItem.setDepth(currentDepth);
                        if (fileItem.getValue().isDirectory()) {
                            queue.add(fileItem);
                        }
                        //System.out.println("Depth level: " + fileItem.getDepth() + ", file item: " + fileItem.getValue().getName());
                    }
                }
            }
        }
    }
}