package lohika;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 14.08.13
 * Time: 21:08
 * To change this template use File | Settings | File Templates.
 */
//public class WordCountReaderImpl extends Reader implements WordCountReader
//{
//	private Reader _reader;
//	private HashSet<Character> _delimiters;
//
//	public WordCountReaderImpl(Reader reader)
//	{
//		_delimiters = new HashSet<Character>(Arrays.asList(new Character[] {
//			' ', '\n', ',', ':', ';'
//		}));
//		_reader = reader;
//	}
//
//	@Override
//	public int read(char[] cbuf, int off, int len)
//			throws IOException
//	{
//		int bytesRead = _reader.read(cbuf, off, len);
//
//		if(bytesRead <= 0)
//			return bytesRead;
//
//
//
//		return bytesRead;
//	}
//
//	@Override
//	public void close()
//			throws IOException
//	{
//		_reader.close();
//	}
//}

public class WordCountReaderImpl {
    private BufferedReader streamReader;

    public WordCountReaderImpl(Reader reader) {
        streamReader = new BufferedReader(reader);
    }

    public WordCountReaderImpl(InputStreamReader streamReader) {
        this.streamReader = new BufferedReader(streamReader);
    }

    public WordCountReaderImpl(InputStream stream) {
        streamReader = new BufferedReader(new InputStreamReader(stream));
    }

    public Map<String, Integer> CountWords() throws IOException {
        String line;
        Map<String, Integer> wordOccurrences = new HashMap<>();

        while ((line = streamReader.readLine()) != null) {
            String[] words = line.split(" |,");

            for (String word : words) {
                Integer count = wordOccurrences.get(word);
                wordOccurrences.put(word, count == null ? 1 : ++count);
            }
        }

        return wordOccurrences;
    }
}
