package lohika;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 14.08.13
 * Time: 21:07
 */
public interface WordCountReader
{
	public Map<String, Integer> CountWords();
}
