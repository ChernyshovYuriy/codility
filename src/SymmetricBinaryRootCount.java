/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 11:17 PM
 */
public class SymmetricBinaryRootCount {

    public static int symmetricBinaryRootCount(int N) {
        int smallestResult = -1;
        for (int i = 1; i <= N >> 1; i++) {
            if (N % i == 0) {
                if (N / i == bit_rev(i)) {
                    if (i < smallestResult || smallestResult == -1) {
                        smallestResult = i;
                    }
                }
            }
        }
        System.out.println("Symmetrical Binary root is " + smallestResult);
        return smallestResult;
    }

    private static int bit_rev(int n) {
        String reversedBin = new StringBuffer(Integer.toBinaryString(n)).reverse().toString();
        return Integer.parseInt(reversedBin, 2);
    }
}