import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 6/23/13
 * Time: 9:11 AM
 */
public class ArraysAndStrings {

    public static void removeDuplicates(int[] array) {
        if (array == null) {
            return;
        }
        int len = array.length;
        if (len < 2) {
            return;
        }
        int tail = 1;
        for (int i = 1; i < len; ++i) {
            int j;
            for (j = 0; j < tail; ++j) {
                //System.out.println("i: " + i + ", j: " + j + ", array[i]: " + array[i] + ", array[j]: " + array[j]);
                if (array[i] == array[j]) {
                    array[i] = -1;
                    break;
                }
            }
            if (j == tail) {
                //System.out.println("   i: " + i + ", tail: " + tail + ", array[i]: " + array[i] + ", array[tail]: " +
                //        array[tail]);
                array[tail] = array[i];
                ++tail;
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (int k : array) {
                stringBuilder.append(String.valueOf(k));
                if (k == -1) {
                    stringBuilder.append(" ");
                } else {
                    stringBuilder.append("  ");
                }
            }
            System.out.println(stringBuilder.toString());
        }
        if (tail < len) {
            array[tail] = -1;
        }
    }

    public static void removeDuplicates(char[] str) {
        if (str == null) {
            return;
        }
        int len = str.length;
        if (len < 2) {
            return;
        }
        int tail = 1;
        for (int i = 1; i < len; i++) {
            int j;
            for (j = 0; j < tail; j++) {
                System.out.println("i: " + i + ", j: " + j + ", str[i]: " + str[i] + ", str[j]: " + str[j]);
                if (str[i] == str[j]) {
                    System.out.println("duplicate: " + str[i]);
                    str[i] = 0;
                    break;
                }
            }
            if (j == tail) {
                str[tail] = str[i];
                tail++;
            }
        }
        //if (tail < len) {
            //str[tail] = 0;
        //}
    }

    public static void findCommonValues_v1(int[] arrayA, int[] arrayB) {
        System.out.println("findCommonValues_v1");
        if (arrayA == null || arrayB == null) {
            return;
        }
        int lengthA = arrayA.length;
        int lengthB = arrayB.length;
        if (lengthA == 0 && lengthB == 0) {
            return;
        }
        int[] arrayC = new int[lengthA > lengthB ? lengthA : lengthB];
        int lengthC = arrayC.length;
        int arrayCInsertPointer = 0;
        int i, j, k;
        for (i = 0; i < lengthA; i++) {
            for (j = 0; j < lengthB; j++) {
                if (arrayA[i] == arrayB[j]) {
                    for (k = 0; k < lengthC; k++) {
                        if (arrayC[k] == arrayA[i]) {
                            break;
                        }
                    }
                    if (k == lengthC) {
                        arrayC[arrayCInsertPointer] = arrayA[i];
                        arrayCInsertPointer++;
                    }
                }
            }
        }
        for (i = 0; i < arrayCInsertPointer; i++) {
            System.out.println("- " + arrayC[i]);
        }
    }

    public static void findCommonValues_v2(int[] arrayA, int[] arrayB) {
        System.out.println("findCommonValues_v2");
        if (arrayA == null || arrayB == null) {
            return;
        }
        int lengthA = arrayA.length;
        int lengthB = arrayB.length;
        if (lengthA == 0 && lengthB == 0) {
            return;
        }
        int i;
        int [] value;
        Hashtable hashtable = new Hashtable();
        for (i = 0; i < lengthA; i++) {
            if (hashtable.get(arrayA[i]) == null) {
                hashtable.put(arrayA[i], new int[]{0, 0});
            }
            value = (int[]) hashtable.get(arrayA[i]);
            value[0]++;
            hashtable.put(arrayA[i], value);
        }
        for (i = 0; i < lengthB; i++) {
            if (hashtable.get(arrayB[i]) == null) {
                hashtable.put(arrayB[i], new int[]{0, 0});
            }
            value = (int[]) hashtable.get(arrayB[i]);
            value[1]++;
            hashtable.put(arrayB[i], value);
        }

        Enumeration<Integer> enumKey = hashtable.keys();
        while (enumKey.hasMoreElements()) {
            Integer key = enumKey.nextElement();
            value = (int[]) hashtable.get(key);
            if (value[0] >= 1 && value[1] >= 1) {
                System.out.println("- " + key);
            }
        }
    }


}