import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 7/5/13
 * Time: 7:30 AM
 */
public class FirstCoveringPrefix {

    public static int getFirstCoveringPrefix(int[] array) {
        int length = array.length;
        if (length < 1 || length > 1000000) {
            return -1;
        }
        HashSet<Integer> temp = new HashSet<Integer>();
        int result = -1;
        for (int i = 0; i < length; i++) {
            if (!temp.contains(array[i])) {
                temp.add(array[i]);
                result = i;
            }
        }
        return result;
    }

}