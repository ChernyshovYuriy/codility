/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 6/23/13
 * Time: 6:43 PM
 */
public class BitsManipulation {

    public static int updateBits(int n, int m, int i, int j) {
        int max = ~0; /* All 1’s */
        System.out.println("N = " + Integer.toBinaryString(n));
        System.out.println("M = " + Integer.toBinaryString(m));
        // 1’s through position j, then 0’s
        int left = max - ((1 << j) - 1);
        System.out.println("left: " + left);
        // 1’s after position i
        int right = ((1 << i) - 1);
        System.out.println("right: " + right);
        // 1’s, with 0s between i and j
        int mask = left | right;
        // Clear i through j, then put m in there
        return (n & mask) | (m << i);
    }

    public static String printBinary(String n) {
        int intPart = Integer.parseInt(n.substring(0, n.indexOf(".")));
        double decPart = Double.parseDouble(n.substring(n.indexOf("."), n.length()));
        String int_string = "";
        while (intPart > 0) {
            int r = intPart % 2;
            intPart >>= 1;
            int_string = r + int_string;
        }
        StringBuilder dec_string = new StringBuilder();
        while (decPart > 0) {
            if (dec_string.length() > 32) {
                return "ERROR";
            }
            if (decPart == 1) {
                dec_string.append((int)decPart);
                break;
            }
            double r = decPart * 2;
            if (r >= 1) {
                dec_string.append(1);
                decPart = r - 1;
            } else {
                dec_string.append(0);
                decPart = r;
            }
        }
        return int_string + "." + dec_string.toString();
    }
}