import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 5/12/13
 * Time: 11:07 PM
 */
public class AnagramOfPalindrome {

    public static int isAnagramOfPalindrome(String s) {
        int len = s.length();
        if (len % 2 == 1) {
            System.out.println("AnagramOfPalindrome is " + 0);
            return 0;
        }
        LinkedList<Integer> toSkip = new LinkedList<Integer>();
        for (int i = 0; i < len - 1; i++) {
            if (toSkip.contains(i)) {
                continue;
            }
            char c = s.charAt(i);
            int otherCharIndex = s.substring(i + 1).indexOf(c);
            if (otherCharIndex == -1) {
                System.out.println("AnagramOfPalindrome is " + 0);
                return 0;
            }
            toSkip.add(i);
            toSkip.add(otherCharIndex + i + 1);
        }
        System.out.println("AnagramOfPalindrome is " + 1);
        return 1;
    }
}