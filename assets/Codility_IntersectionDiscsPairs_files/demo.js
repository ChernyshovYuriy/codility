function debug(msg) {	
	$('#debug').html(msg);
}

var Demo = {
  arrowHnd: null,
  arrowRHnd: null,
  arrowTarget: null,
  arrowTargetPos: null,
  arrowObj: null,
  arrowImgObj: null,
  arrowDir: null,
  arrowBounceDir: null,
  arrowInAnim: false,
  arrowDX: 0,
  arrowDY: 0,
		
  setupArrow: function(elem, arrowElemId, dir, dx,dy) {
	  this.arrowTarget = $(elem);
	  this.arrowObj = $(arrowElemId);
	  this.arrowImgObj = $(''+arrowElemId+' img');
	  this.arrowDir = dir;	  	  	  
	  $(window).resize(function() { Demo.doMoveArrow(); });
	  this.arrowHnd = setTimeout(function() { Demo.doArrowBounce(); }, 200);
	  if (dx) this.arrowDX=dx;
	  if (dy) this.arrowDY=dy;
  },
  
  doMoveArrow: function() {
	  if (this.arrowInAnim) {
		  clearTimeout(this.demoRHnd);
		  this.demoRHnd = setTimeout(function() { Demo.doMoveArrow(); }, 100);
	  }
	  
	  var p = $(this.arrowTarget).position();
	  if (!p) {
		  $(this.arrowObj).hide();
		  return;
	  }
	  
	  $(this.arrowObj).show();
	  	  
	  var el_w = $(this.arrowTarget).width();
	  var el_h = $(this.arrowTarget).height();
	  var arr_w = $(this.arrowObj).width();
	  var arr_h = $(this.arrowObj).height();
	  var l,t;
	  
	  if (this.arrowDir=='down') {
		  l = p.left + el_w/2 - arr_w/2;
		  t = p.top - arr_h;
	  } else if (this.arrowDir=='right') {
		  l = p.left - arr_w;
		  t = p.top + el_w/2 - arr_h/2;
	  }
	  
	  l += this.arrowDX;
	  t += this.arrowDY;
	  
	  if (l<=0) return;
	  	  
	  $(this.arrowObj).css({left: l,top: t});	  
	  	  
	  if (!this.arrowInAnim)
		  this.arrowTargetPos = p;
  },
  
  doArrowBounce: function() {
	  if (!this.arrowObj) return;
	  
	  var old_p = this.arrowTargetPos;
	  var cur_p = $(this.arrowTarget).position();
	  if (old_p==null || cur_p.left!=old_p.left || cur_p.top!=old_p.top)
		  this.doMoveArrow();
	  
	  $(this.arrowObj).stop();
	  this.arrowInAnim = true;	  
	  $(this.arrowImgObj).effect("bounce",{direction: this.arrowDir, distance: 20, times: 3}, 300,function() { Demo.arrowInAnim=false;});	  	  
	  if (this.arrowHnd) clearTimeout(this.arrowHnd);	  
	  this.arrowHnd = setTimeout(function() { Demo.doArrowBounce(); }, 3000);
  },
  		
  addHelp: function (content_div, location_elem, dir, dynamic, dx, dy, bg) {
	var corner_tip, corner_target, tip_x=15, tip_y=15;
        if (!bg) bg='#990100';
	if (!dx) dx=0;
	if (!dy) dy=0;
	if (dir=='left') {
		corner_target = 'leftTop';
		corner_tip = 'rightBottom';		
		tip_x=30;
	} else if (dir=='leftTop') {
		corner_target = 'leftBottom';
		corner_tip = 'rightTop';	
		tip_x=30;
	} else if (dir=='leftBottom') {
		corner_target = 'leftBottom';
		corner_tip = 'rightBottom';		
		tip_x=30;		
	} else if (dir=='top') {
		corner_target = 'topMiddle';
		corner_tip = 'bottomLeft';
	} else if (dir=='topMiddle') {
		corner_target = 'topMiddle';
		corner_tip = 'bottomMiddle';
	} else if (dir=='topNoTip') {
		corner_target = 'topMiddle';
		corner_tip = 'bottomMiddle';
		tip_x = 0;
		tip_y = 0;
	} else if (dir=='bottom') {
		corner_target = 'bottomMiddle';
        corner_tip = 'topMiddle';
	} else if (dir=='bottomRight') {
		corner_target = 'bottomMiddle';
        corner_tip = 'topLeft';
	}
	var attr_show = { ready: true, when: { target: false, event:'mouseover' }, effect: { type: 'fade', length: 50 } };
	var attr_hide = false;
	
	if (dynamic) {
		attr_show = { when: { target: $(location_elem), event:'mouseover'}, effect: { type: 'fade', length: 50 } };
		attr_hide = { when: { target: $(location_elem), event:'mouseout'}, effect: { type: 'fade', length: 50 } };
	}
	
	$(location_elem).qtip({
		content: {			
			text: $(content_div).html()
	    },
		show: attr_show,
		hide: attr_hide, 
		style: { 
			color: 'white',
			background: bg,
			fontWeight: 'bold',
			textAlign: 'center',
			tip: { corner: corner_tip, size: {x: tip_x, y: tip_y } }, 
			width: { max: 350 },
		    border: {
				width: 2,
				radius: 10,
				color: bg,
			}
		},
		position: {
		   adjust: { x: dx, y: dy },
		   corner: {
		     target: corner_target,
		     tooltip: corner_tip
		   }
		}
	});
  }
};

