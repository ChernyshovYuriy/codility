/**
 * downloaded from http://www.jason-palmer.com/demo/jquery-form-defaults/
 * modified by TW, 2009.03.06
 */
jQuery.fn.DefaultValue = function(text){
    return this.each(function(){
        var def_val_color = '#888';
        var def_font_style = 'italic';        
        var mod_val_color = '#000';
        var mod_font_style = 'normal';

        //Make sure we're dealing with text-based form fields
        if(this.type != 'text' && this.type != 'password' && this.type != 'textarea')
            return;

        //Store field reference
        var fld_current=this;

        //Set value initially if none are specified
        if(this.value=='' || this.value==text) {
            this.value=text;
            this.style.color=def_val_color;
            this.style.fontStyle=def_font_style;
        } else {
            //Other value exists - ignore
            return;
        }

        //Remove values on focus
        $(this).focus(function() {
            if(this.value==text) this.value='';
            this.style.color=mod_val_color;
            this.style.fontStyle=mod_font_style;
        });

        $(this).change(function() {
            if(this.value==text) {
                this.style.color=def_val_color;
                this.style.fontStyle=def_font_style;
            } else {
                this.style.color=mod_val_color;
                this.style.fontStyle=mod_font_style;
            }
        });

        //Place values back on blur
        $(this).blur(function() {
            if(this.value==text || this.value=='') {
                this.value=text;
                this.style.color=def_val_color;
                this.style.fontStyle=def_font_style;
            }
        });

        //Capture parent form submission
        //Remove field values that are still default
        $(this).parents("form").each(function() {
            //Bind parent form submit
            $(this).submit(function() {
                if(fld_current.value==text) {
                    fld_current.value='';
                }
            });
        });
    });
};