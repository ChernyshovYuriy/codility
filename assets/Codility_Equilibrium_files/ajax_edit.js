function setupAjaxEditor(name, elem_id, editor_id, post_url) {
	var elem = $(elem_id);
	var editor = $(editor_id);
	
	$(editor_id+' a.cancel').click(function() {
		$(editor_id).hide();
		$(elem_id).show();
		return false;
	});		
	
	$(elem_id+' a.change').click(function() {		
		$(elem_id).hide();
		$(editor_id).show();
		return false;
	});
	
	$(editor_id+' .submit').click(function() {
		var i = $(editor_id+' input[name="value"]');
		if (i.length==0) i = $(editor_id+' textarea[name="value"]');
		var v = $(i).val();
		$(elem_id+' span.value').html($('<div/>').text(v).html());
		var data = {'field':name,'value':v};
		var csrf = $(editor_id+' input[name="csrfmiddlewaretoken"]');
		if (csrf) {
		    data['csrfmiddlewaretoken'] = csrf.val();
		}
		var args = { type:"POST", url: post_url, data: data};
	    $.ajax(args);
		
		$(editor_id).hide();
		$(elem_id).show();
	});
}