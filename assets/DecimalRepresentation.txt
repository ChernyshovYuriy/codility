Each rational number A/B has a decimal representation of the form "R", "R.Q" or "R.Q(P)" where R, Q and P are
(possibly empty) strings consisting of the characters '0'-'9'. For example:
a decimal representation of 6/3 is "2.00(0000)";
a decimal representation of 1/3 is "0.333(333)";
a decimal representation of 1/4 is "0.25(0)";
a decimal representation of 1/7 is "0.(142857)".
Any given rational number A/B has many decimal representations. Exactly one of those representations is the shortest.
It can be characterized as follows:
a trailing ".0" is omitted for numbers A/B that are integers;
a trailing "(0)" is always omitted;
a trailing "(9)" is forbidden;
a representation of the form "R.Q(P)" is the shortest if it consists of the shortest possible strings Q and P.
For example, these are the shortest decimal representations of a few chosen rational numbers:
the shortest decimal representation of 3/1 is "3";
the shortest decimal representation of 4/2 is "2";
the shortest decimal representation of 5/2 is "2.5";
the shortest decimal representation of 3/4 is "0.75";
the shortest decimal representation of 11/10 is "1.1";
the shortest decimal representation of 1/3 is "0.(3)";
the shortest decimal representation of 3/28 is "0.10(714285)".
Write a function
class Solution { public String solution(int A,int B); }
that, given two positive integers A and B, returns the decimal representation of the rational number A/B.
For example:
for A = 12 and B = 3 the function should return "4";
for A = 1 and B = 2 the function should return "0.5"};
for A = 5 and B = 4 the function should return "1.25";
for A = 1 and B = 3 the function should return "0.(3)";
for A = 3 and B = 28 the function should return "0.10(714285)".
Assume that:
A is an integer within the range [0..1,000,000];
B is an integer within the range [0..1,000,000].
Complexity:
expected worst-case time complexity is O(A);
expected worst-case space complexity is O(1).

http://stackoverflow.com/questions/3730019/why-not-use-double-or-float-to-represent-currency
http://stackoverflow.com/questions/14068429/shortest-decimal-representations-of-rational-numbers